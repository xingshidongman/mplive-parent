package com.kalix.mplive.answeringQuestion.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.answeringQuestion.api.dao.IVoteBeanDao;
import com.kalix.mplive.answeringQuestion.entities.VoteBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 功能描述:
 *
 * @Description: 投票数据访问层实现类
 * @Author: whw
 * @CreateDate: 2019/11/7
 * @Version:
 */
public class VoteBeanDaoImpl extends GenericDao<VoteBean, Long> implements IVoteBeanDao {
    @Override
    @PersistenceContext(unitName = "mplive-answeringQuestion-unit")
    public void setEntityManager(EntityManager em) {

        super.setEntityManager(em);
    }
}
