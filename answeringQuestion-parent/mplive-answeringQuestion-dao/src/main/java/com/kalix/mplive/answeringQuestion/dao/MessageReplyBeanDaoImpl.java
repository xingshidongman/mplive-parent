package com.kalix.mplive.answeringQuestion.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.answeringQuestion.api.dao.IMessageReplyBeanDao;
import com.kalix.mplive.answeringQuestion.entities.MessageReplyBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 功能描述:
 *
 * @Description: 留言回复数据访问层
 * @Author: whw
 * @CreateDate: 2019/10/25$ 10:45$
 * @Version:
 */
public class MessageReplyBeanDaoImpl extends GenericDao<MessageReplyBean, Long> implements IMessageReplyBeanDao {
    @Override
    @PersistenceContext(unitName = "mplive-answeringQuestion-unit")
    public void setEntityManager(EntityManager em) {

        super.setEntityManager(em);
    }
    @Override
    public void deleteAllByPostid(long id) {
        String sql = "delete from mplive_MessageReply r where r.messageId='"+id+"'";
        entityManager.createNativeQuery(sql).executeUpdate();
    }
}
