package com.kalix.mplive.answeringQuestion.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.answeringQuestion.api.dao.IMessageBeanDao;
import com.kalix.mplive.answeringQuestion.entities.MessageBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 功能描述:
 *
 * @Description: 论坛数据访问层实现类
 * @Author: whw
 * @CreateDate: 2019/10/23$ 16:47$
 * @Version:
 */
public class MessageBeanDaoImpl extends GenericDao<MessageBean, Long> implements IMessageBeanDao {
    @Override
    @PersistenceContext(unitName = "mplive-answeringQuestion-unit")
    public void setEntityManager(EntityManager em) {

        super.setEntityManager(em);
    }
}
