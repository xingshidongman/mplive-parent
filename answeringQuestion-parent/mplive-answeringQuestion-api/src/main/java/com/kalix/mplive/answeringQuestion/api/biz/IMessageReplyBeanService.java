package com.kalix.mplive.answeringQuestion.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.mplive.answeringQuestion.api.dto.MessageReplyTreeDTO;
import com.kalix.mplive.answeringQuestion.api.dto.MessageReplyTreeHDTO;
import com.kalix.mplive.answeringQuestion.entities.MessageReplyBean;

/**
 * 功能描述:
 *
 * @Description: 留言回复服务层
 * @Author: whw
 * @CreateDate: 2019/10/25$ 10:20$
 * @Version:
 */
/**
 * 功能描述：新增回复留言接口及留言回复的回复接口 及之下接口
 *
 * @Author: Wanghw
 * @Date: 2019/10/31
 * @Version V1.0
 *
**/
public interface IMessageReplyBeanService extends IBizService<MessageReplyBean> {
    // 回复树
    MessageReplyTreeDTO getMessageReplyByPostId(Long messageId, String jsonStr);
    // 回复树 后台
    MessageReplyTreeHDTO getMessageReplyByPostIdH(Long messageId, String jsonStr);


    // 留言管理的删除帖子以及回复的功能
    void deleteAllByPostid(long id);

    // 回复留言
    void getReplyMessage(String userName ,Long messageId,String replyMessage,String photo,String headportrait,String identity,Boolean judgeIsScore,long userId,int fraction);

    // 后台增加回复留言
    JsonStatus addReplyMessage(MessageReplyBean messageReply);

    //回复留言的留言
    void getComment(String userName,Long messageId,String replyMessage ,String photo,Long replyID,String headportrait,String identity,String repeatName);

    // 查看回复的回复
    JsonData lookMessage(Long messageId, Long replyID);

    // 个人回复帖子信息
    JsonData getAddComment(Integer uid,Integer num);

    // 获取回帖总数
    JsonData getMessageReplysum();
    // 根据页数返回发帖列表
    JsonData getMessageReplyPostinglist(Integer id,Integer num);

    // 根据帖子ID查询回复内容
    JsonData getReplydetails(Long messageId);

    JsonStatus downloadFile(String file);
    JsonData judgeIsScore(long id, long userId);
}
