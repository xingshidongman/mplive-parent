package com.kalix.mplive.answeringQuestion.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * 功能描述:
 *
 * @Description: 留言DTO
 * @Author: yyp
 * @CreateDate: 2019/10/24$ 09:10$
 * @Version:
 */
public class MessageDTO {
    private long id;
    private String userName;// 用户名称
    private String icon; //用户头像
    private String messageTitle; // 留言标题
    private String messageContent;  // 评论内容
    @ApiModelProperty(value = "创建日期", hidden = true)
    private Date messageDateTime; // 创建日期
    private boolean messageFlag; // 标识是否置顶
    private boolean messageJingFlag; // 标识是否为“精”
    private int counts;
    private String photo;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public boolean isMessageFlag() {
        return messageFlag;
    }

    public void setMessageFlag(boolean messageFlag) {
        this.messageFlag = messageFlag;
    }

    public boolean isMessageJingFlag() {
        return messageJingFlag;
    }

    public void setMessageJingFlag(boolean messageJingFlag) {
        this.messageJingFlag = messageJingFlag;
    }

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getMessageDateTime() {
        return messageDateTime;
    }

    public void setMessageDateTime(Date messageDateTime) {
        this.messageDateTime = messageDateTime;
    }
}
