package com.kalix.mplive.answeringQuestion.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.answeringQuestion.entities.CarouselBean;
import com.kalix.mplive.answeringQuestion.entities.VoteBean;


/**
 * 功能描述:
 *
 * @Description: 轮播数据访问层
 * @Author: whw
 * @CreateDate: 2019/11/7
 * @Version:
 */
public interface ICarouselBeanDao extends IGenericDao<CarouselBean, Long> {
}
