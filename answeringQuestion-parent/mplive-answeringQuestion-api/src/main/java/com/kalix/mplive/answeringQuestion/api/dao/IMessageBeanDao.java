package com.kalix.mplive.answeringQuestion.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.answeringQuestion.entities.MessageBean;


/**
 * 功能描述:
 *
 * @Description: 留言数据访问层
 * @Author: whw
 * @CreateDate: 2019/10/23$ 16:14$
 * @Version:
 */
public interface IMessageBeanDao extends IGenericDao<MessageBean, Long> {
}
