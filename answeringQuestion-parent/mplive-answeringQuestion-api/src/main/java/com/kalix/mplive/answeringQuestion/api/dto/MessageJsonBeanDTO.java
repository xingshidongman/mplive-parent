package com.kalix.mplive.answeringQuestion.api.dto;

/**
 * 功能描述:
 *
 * @Description: 回复DTO
 * @Author: yyp
 * @CreateDate: 2019/10/24$ 09:55$
 * @Version:
 */
public class MessageJsonBeanDTO {
    private long value;
    private String label;
    private String replyMessage;
    private String userName;
    private long replyNum;
    private long uid;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getReplyNum() {
        return replyNum;
    }

    public void setReplyNum(long replyNum) {
        this.replyNum = replyNum;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getReplyMessage() {
        return replyMessage;
    }

    public void setReplyMessage(String replyMessage) {
        this.replyMessage = replyMessage;
    }
}
