package com.kalix.mplive.answeringQuestion.api.dto;

import com.kalix.framework.core.api.web.model.BaseDTO;
import com.kalix.mplive.answeringQuestion.entities.MessageReplyBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述:
 *
 * @Description: 回复树DTO
 * @Author: whw
 * @CreateDate: 2019/10/25$ 10:23$
 * @Version:
 */
public class MessageReplyTreeDTO extends BaseDTO {
    private Long userId; // 回复用户ID
    private String userName; //回复用户姓名
    private String replyMessage;  //回复内容
    private Boolean leaf; //是否是叶子节点
    private Long parentId;  // 父节点
    private String parentName; //父节点姓名
    private long messageId; // 留言id
    private List<MessageReplyTreeDTO> children = new ArrayList<MessageReplyTreeDTO>(); //子回复
//    private List<MessageReplyBean> children1 = new ArrayList<MessageReplyBean>(); //子回复

//    public List<MessageReplyBean> getChildren1() {
//        return children1;
//    }
//
//    public void setChildren1(List<MessageReplyBean> children1) {
//        this.children1 = children1;
//    }

    public List<MessageReplyTreeDTO> getChildren() {
        return children;
    }

    public void setChildren(List<MessageReplyTreeDTO> children) {
        this.children = children;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getReplyMessage() {
        return replyMessage;
    }

    public void setReplyMessage(String replyMessage) {
        this.replyMessage = replyMessage;
    }

    public Boolean getLeaf() {
        return leaf;
    }

    public void setLeaf(Boolean leaf) {
        this.leaf = leaf;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }
}
