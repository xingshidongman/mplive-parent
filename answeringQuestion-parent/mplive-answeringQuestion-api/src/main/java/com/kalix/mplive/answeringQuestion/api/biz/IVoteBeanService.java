package com.kalix.mplive.answeringQuestion.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.mplive.answeringQuestion.entities.VoteBean;


/**
 * 功能描述:
 *
 * @Description: 投票服务层
 * @Author: whw
 * @CreateDate: 2019/11/7$
 * @Version:
 */
public interface IVoteBeanService extends IBizService<VoteBean> {

}
