package com.kalix.mplive.answeringQuestion.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.mplive.answeringQuestion.entities.CarouselBean;


/**
 * 功能描述:
 *
 * @Description: 轮播服务层
 * @Author: whw
 * @CreateDate: 2019/11/7$
 * @Version:
 */
public interface ICarouselBeanService extends IBizService<CarouselBean> {

}
