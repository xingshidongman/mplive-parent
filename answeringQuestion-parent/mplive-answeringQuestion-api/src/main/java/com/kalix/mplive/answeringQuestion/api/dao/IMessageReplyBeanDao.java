package com.kalix.mplive.answeringQuestion.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.answeringQuestion.entities.MessageReplyBean;

/**
 * 功能描述:
 *
 * @Description: 留言回复数据访问层
 * @Author: whw
 * @CreateDate: 2019/10/25$ 10:26$
 * @Version:
 */
public interface IMessageReplyBeanDao extends IGenericDao<MessageReplyBean, Long> {
    void deleteAllByPostid(long id);
}
