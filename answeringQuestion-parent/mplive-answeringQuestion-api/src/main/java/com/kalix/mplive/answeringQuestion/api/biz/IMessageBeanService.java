package com.kalix.mplive.answeringQuestion.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.mplive.answeringQuestion.entities.MessageBean;


/**
 * 功能描述:
 *
 * @Description: 留言服务层
 * @Author: yyp
 * @CreateDate: 2019/10/23$ 16:07$
 * @Version:
 */
/**
  * 功能描述： 评星之下都是我写的
  *
  * @Author: Wanghw
  * @Date: 2019/11/11
  * @Version V1.0
  *
 **/
public interface IMessageBeanService extends IBizService<MessageBean> {
     //获取留言
    JsonData getMessage();
    //回复管理左侧菜单
    JsonData getMessageReplyForTree();
    //删除帖子及回复的功能
    void deleteAllById(long id);
    // 增加发帖
    JsonStatus addMessage(Integer uid,String messageContent,String messageTitle,String photo,String file,String headportrait,String identity);
    // 评星
    String getFraction(long id,long fraction);
    //获取当前人所有发帖
    JsonData getAddMessage(Integer uid,Integer num);
    // 浏览人数
    String getbrowsenum(long id);
    // 评论人数
    String getreplynum(long id);
    // 是否投票
    String getwhethertovote(long id);
    // 获取发帖总数
    JsonData getsum();
    // 根据页数返回发帖列表
    JsonData getPostinglist(int num);
    //根据帖子ID查询帖子详情
    JsonData getForumdetails(Long id);
    //后台删除回复，回复数量减一
}
