package com.kalix.mplive.answeringQuestion.entities;

import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 功能描述：
 * 轮播实体类
 * @Author: Wanghw
 * @Date: 2019/11/11
 * @Version V1.0
 *
**/
@Entity
@Table(name = "mplive_carouse")
public class CarouselBean extends PersistentEntity {
    private String photourl;
    private String photo;

    public String getUrl() {
        return photourl;
    }

    public void setUrl(String photourl) {
        this.photourl = photourl;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
