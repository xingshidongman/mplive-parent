package com.kalix.mplive.answeringQuestion.entities;

import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 功能描述:
 *
 * @Description: 投票实体类
 * @Author: whw
 * @CreateDate: 2019/11/7$ 15:36$
 * @Version:
 */
@Entity
@Table(name = "mplive_vote")
public class VoteBean extends PersistentEntity {
    private String votetitle; //投票主题
    private String option1; //
    private String option1num; //
    private String option2; //
    private String option2num; //
    private String option3; //
    private String option3num; //
    private String option4; //
    private String option4num; //
    private String option5; //
    private String option5num; //
    private String option6; //
    private String option6num; //
    private String option7; //
    private String option7num; //
    private String option8; //
    private String option8num; //
    private String option9; //
    private String option9num; //
    private String option10; //
    private String option10num; //
    private String option11; //
    private String option11num; //
    private String option12; //
    private String option12num; //
    private String option13; //
    private String option13num; //
    private String option14; //
    private String option14num; //
    private String option15; //
    private String option15num; //
    private String deadline; //截止时间
    private boolean multi;//是否是多选

    public String getVotetitle() {
        return votetitle;
    }

    public void setVotetitle(String votetitle) {
        this.votetitle = votetitle;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption1num() {
        return option1num;
    }

    public void setOption1num(String option1num) {
        this.option1num = option1num;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption2num() {
        return option2num;
    }

    public void setOption2num(String option2num) {
        this.option2num = option2num;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption3num() {
        return option3num;
    }

    public void setOption3num(String option3num) {
        this.option3num = option3num;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }

    public String getOption4num() {
        return option4num;
    }

    public void setOption4num(String option4num) {
        this.option4num = option4num;
    }

    public String getOption5() {
        return option5;
    }

    public void setOption5(String option5) {
        this.option5 = option5;
    }

    public String getOption5num() {
        return option5num;
    }

    public void setOption5num(String option5num) {
        this.option5num = option5num;
    }

    public String getOption6() {
        return option6;
    }

    public void setOption6(String option6) {
        this.option6 = option6;
    }

    public String getOption6num() {
        return option6num;
    }

    public void setOption6num(String option6num) {
        this.option6num = option6num;
    }

    public String getOption7() {
        return option7;
    }

    public void setOption7(String option7) {
        this.option7 = option7;
    }

    public String getOption7num() {
        return option7num;
    }

    public void setOption7num(String option7num) {
        this.option7num = option7num;
    }

    public String getOption8() {
        return option8;
    }

    public void setOption8(String option8) {
        this.option8 = option8;
    }

    public String getOption8num() {
        return option8num;
    }

    public void setOption8num(String option8num) {
        this.option8num = option8num;
    }

    public String getOption9() {
        return option9;
    }

    public void setOption9(String option9) {
        this.option9 = option9;
    }

    public String getOption9num() {
        return option9num;
    }

    public void setOption9num(String option9num) {
        this.option9num = option9num;
    }

    public String getOption10() {
        return option10;
    }

    public void setOption10(String option10) {
        this.option10 = option10;
    }

    public String getOption10num() {
        return option10num;
    }

    public void setOption10num(String option10num) {
        this.option10num = option10num;
    }

    public String getOption11() {
        return option11;
    }

    public void setOption11(String option11) {
        this.option11 = option11;
    }

    public String getOption11num() {
        return option11num;
    }

    public void setOption11num(String option11num) {
        this.option11num = option11num;
    }

    public String getOption12() {
        return option12;
    }

    public void setOption12(String option12) {
        this.option12 = option12;
    }

    public String getOption12num() {
        return option12num;
    }

    public void setOption12num(String option12num) {
        this.option12num = option12num;
    }

    public String getOption13() {
        return option13;
    }

    public void setOption13(String option13) {
        this.option13 = option13;
    }

    public String getOption13num() {
        return option13num;
    }

    public void setOption13num(String option13num) {
        this.option13num = option13num;
    }

    public String getOption14() {
        return option14;
    }

    public void setOption14(String option14) {
        this.option14 = option14;
    }

    public String getOption14num() {
        return option14num;
    }

    public void setOption14num(String option14num) {
        this.option14num = option14num;
    }

    public String getOption15() {
        return option15;
    }

    public void setOption15(String option15) {
        this.option15 = option15;
    }

    public String getOption15num() {
        return option15num;
    }

    public void setOption15num(String option15num) {
        this.option15num = option15num;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public boolean isMulti() {
        return multi;
    }

    public void setMulti(boolean multi) {
        this.multi = multi;
    }
}
