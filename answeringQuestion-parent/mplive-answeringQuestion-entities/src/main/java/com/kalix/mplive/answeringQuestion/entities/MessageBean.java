package com.kalix.mplive.answeringQuestion.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * 功能描述:
 *
 * @Description: 留言实体类
 * @Author: whw
 * @CreateDate: 2019/11/8
 * @Version:
 */
@Entity
@Table(name = "mplive_Message")
public class MessageBean extends PersistentEntity {
    private long uid; // 用户
    private String userName; // 用户姓名
    @Column(name = "messageContent", columnDefinition = "text")
    private String messageContent;  // 评论内容
    private Date messageDateTime; // 获取发帖时间
    private boolean messageFlag; // 标识是否置顶
    private boolean messageJingFlag; // 标识是否为“精”
    private String messageTitle; // 留言标题
    private String photo; // 图片
    private Long fraction; // 分数
    private Long number; // 人数
    private String file; // 文件
    private long browsenum; // 浏览人数
    private long replynum; // 回复人数
    private boolean whethertovote; // 是否投票
    private String headportrait; // 头像
    private String identity; //身份
    @Column(name = "scoreArray", columnDefinition = "text")
    private String scoreArray; // 评分数组

    public String getScoreArray() {
        return scoreArray;
    }

    public void setScoreArray(String scoreArray) {
        this.scoreArray = scoreArray;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getHeadportrait() {
        return headportrait;
    }

    public void setHeadportrait(String headportrait) {
        this.headportrait = headportrait;
    }

    public long getBrowsenum() {
        return browsenum;
    }

    public void setBrowsenum(long browsenum) {
        this.browsenum = browsenum;
    }

    public long getReplynum() {
        return replynum;
    }

    public void setReplynum(long replynum) {
        this.replynum = replynum;
    }

    public boolean isWhethertovote() {
        return whethertovote;
    }

    public void setWhethertovote(boolean whethertovote) {
        this.whethertovote = whethertovote;
    }

    public Long getFraction() {
        return fraction;
    }

    public void setFraction(Long fraction) {
        this.fraction = fraction;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public boolean isMessageJingFlag() {
        return messageJingFlag;
    }

    public void setMessageJingFlag(boolean messageJingFlag) {
        this.messageJingFlag = messageJingFlag;
    }

    public boolean isMessageFlag() {
        return messageFlag;
    }

    public void setMessageFlag(boolean messageFlag) {
        this.messageFlag = messageFlag;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getMessageDateTime() {
        return messageDateTime;
    }

    public void setMessageDateTime(Date messageDateTime) {
        this.messageDateTime = messageDateTime;
    }
}
