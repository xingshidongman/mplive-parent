package com.kalix.mplive.answeringQuestion.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * 功能描述:
 *
 * @Description: 帖子回复
 * @Author: whw
 * @CreateDate: 2019/10/24$ 17:11$
 * @Version:
 */
@Entity
@Table(name = "mplive_MessageReply")
public class MessageReplyBean extends PersistentEntity {
    private Long messageId; // 留言id
    @Column(name = "replyMessage", columnDefinition = "text")
    private String replyMessage; //回复内容
    private Long userId; // 回复用户ID
    private String userName; // 回复用户姓名
    private Long parentId; //父ID
    private Long isLeaf;    //是否是子节点
    private String photo;
    private Long replyID; //回复ID
    private String headportrait; //头像
    private String identity; //身份
    private Date messageDateTime; // 获取回复时间
    private String repeatName; //回复于XX的名字
    private Boolean judgeIsScore; //标识用户是否已经评分
    private int fraction;

    public Boolean getJudgeIsScore() {
        return judgeIsScore;
    }

    public void setJudgeIsScore(Boolean judgeIsScore) {
        this.judgeIsScore = judgeIsScore;
    }

    public int getFraction() {
        return fraction;
    }

    public void setFraction(int fraction) {
        this.fraction = fraction;
    }

    public String getRepeatName() {
        return repeatName;
    }

    public void setRepeatName(String repeatName) {
        this.repeatName = repeatName;
    }

    public Date getMessageDateTime() {
        return messageDateTime;
    }
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public void setMessageDateTime(Date messageDateTime) {
        this.messageDateTime = messageDateTime;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getHeadportrait() {
        return headportrait;
    }

    public void setHeadportrait(String headportrait) {
        this.headportrait = headportrait;
    }

    public Long getReplyID() {
        return replyID;
    }

    public void setReplyID(Long replyID) {
        this.replyID = replyID;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public String getReplyMessage() {
        return replyMessage;
    }

    public void setReplyMessage(String replyMessage) {
        this.replyMessage = replyMessage;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getIsLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(Long isLeaf) {
        this.isLeaf = isLeaf;
    }
}
