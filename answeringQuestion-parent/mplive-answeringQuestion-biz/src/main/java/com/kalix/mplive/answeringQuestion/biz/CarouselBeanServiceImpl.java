package com.kalix.mplive.answeringQuestion.biz;

import com.kalix.framework.core.impl.biz.ShiroGenericBizServiceImpl;
import com.kalix.mplive.answeringQuestion.api.biz.ICarouselBeanService;
import com.kalix.mplive.answeringQuestion.api.dao.ICarouselBeanDao;
import com.kalix.mplive.answeringQuestion.entities.CarouselBean;


/**
 * 功能描述:
 *
 * @Description: 轮播实现类
 * @Author: whw
 * @CreateDate: 2019/11/7$
 * @Version:
 */
public class CarouselBeanServiceImpl extends ShiroGenericBizServiceImpl<ICarouselBeanDao, CarouselBean> implements ICarouselBeanService {

}
