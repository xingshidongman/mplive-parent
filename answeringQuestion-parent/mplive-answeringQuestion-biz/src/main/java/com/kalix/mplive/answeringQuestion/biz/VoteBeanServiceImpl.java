package com.kalix.mplive.answeringQuestion.biz;

import com.kalix.framework.core.impl.biz.ShiroGenericBizServiceImpl;
import com.kalix.mplive.answeringQuestion.api.biz.IVoteBeanService;
import com.kalix.mplive.answeringQuestion.api.dao.IVoteBeanDao;
import com.kalix.mplive.answeringQuestion.entities.VoteBean;


/**
 * 功能描述:
 *
 * @Description: 投票实现类
 * @Author: whw
 * @CreateDate: 2019/11/7$
 * @Version:
 */
public class VoteBeanServiceImpl extends ShiroGenericBizServiceImpl<IVoteBeanDao, VoteBean> implements IVoteBeanService {

}
