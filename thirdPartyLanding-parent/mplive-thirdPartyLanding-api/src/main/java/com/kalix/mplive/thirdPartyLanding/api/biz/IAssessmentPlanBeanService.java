package com.kalix.mplive.thirdPartyLanding.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.mplive.thirdPartyLanding.entities.AssessmentPlanBean;
import java.security.GeneralSecurityException;
//import com.qq.connect.QQConnectException;


/**
 * 功能描述:
 *
 * @Description: 第三方登陆
 * @Author: whw
 * @CreateDate: 2019/10/23$ 09:05$
 * @Version:
 */
public interface IAssessmentPlanBeanService extends IBizService<AssessmentPlanBean> {

    // qq登陆
//    JsonData qqLogin(String openID, String accessToken ) throws QQConnectException;

    // 添加用户信息
    String adduser(Long id,String name,String sex,String email,String telephone,String headportrait);

    // 根据学号查询个人信息
    JsonData getStudentCode(Integer code);

    // 根据学号查询个人信息
    JsonData getStudentinformation(Long uid);

    String updateUserinfo(String name,String sex, String phone , String email, String icon , Long id,Integer code);

}
