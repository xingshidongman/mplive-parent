package com.kalix.mplive.thirdPartyLanding.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.thirdPartyLanding.entities.AssessmentPlanBean;


/**
 * 功能描述:
 *
 * @Description: 第三方登陆
 * @Author: whw
 * @CreateDate: 2019/10/23$ 09:08$
 * @Version:
 */
public interface IAssessmentPlanBeanDao extends IGenericDao<AssessmentPlanBean, Long> {
}
