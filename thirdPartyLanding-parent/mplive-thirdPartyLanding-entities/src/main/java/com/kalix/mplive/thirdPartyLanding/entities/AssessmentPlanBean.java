package com.kalix.mplive.thirdPartyLanding.entities;

import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 功能描述:
 *
 * @Description: 第三方登陆
 * @Author: whw
 * @CreateDate: 2019/11/8
 * @Version:
 */
@Entity
@Table(name = "mplive_AssessmentPlan")
public class AssessmentPlanBean extends PersistentEntity {
    private  String name; //姓名
    private  String sex; //性别
    private  String email; //邮箱
    private  String telephone; //电话
    private  String headportrait; //头像
    private  Long uid; // user表中id
    private  Long studentId; // 学号

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getHeadportrait() {
        return headportrait;
    }

    public void setHeadportrait(String headportrait) {
        this.headportrait = headportrait;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
