package com.kalix.mplive.thirdPartyLanding.biz;


import com.kalix.admin.core.entities.UserBean;
import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.framework.core.impl.biz.ShiroGenericBizServiceImpl;

import com.kalix.framework.core.util.ConfigUtil;
import com.kalix.mplive.thirdPartyLanding.api.biz.IAssessmentPlanBeanService;
import com.kalix.mplive.thirdPartyLanding.api.dao.IAssessmentPlanBeanDao;
import com.kalix.mplive.thirdPartyLanding.entities.AssessmentPlanBean;
import com.qq.connect.QQConnectException;
import com.qq.connect.api.qzone.UserInfo;
import com.qq.connect.javabeans.qzone.UserInfoBean;
import com.sun.mail.util.MailSSLSocketFactory;

import java.util.List;
import java.util.Properties;
import java.util.Random;


/**
 * 功能描述:
 *
 * @Description: 第三方登陆
 * @Author: whw
 * @CreateDate: 2019/10/23$ 09:11$
 * @Version:
 */
public class AssessmentPlanBeanServiceImpl extends ShiroGenericBizServiceImpl<IAssessmentPlanBeanDao, AssessmentPlanBean> implements IAssessmentPlanBeanService {

    /**
     * qq 第三方登陆
     * @param openID
     * @param accessToken
     * @return
     * @throws QQConnectException
     */
//    @Override
//    public JsonData qqLogin(String openID, String accessToken) throws QQConnectException {
//
//        // 通过OpenID获取QQ用户登录信息对象(Oppen_ID代表着QQ用户的唯一标识)
//        UserInfo qzoneUserInfo = new UserInfo(accessToken, openID);
//        // 获取用户信息对象(userInfoBean中包含了我们想要的一些信息，比如：nickename、Gender、头像)
//        UserInfoBean userInfoBean = qzoneUserInfo.getUserInfo();
//        System.out.println(userInfoBean.getAvatar());
//        System.out.println("性别: "+userInfoBean.getGender());
//        System.out.println("等级: "+userInfoBean.getLevel());
//        System.out.println("消息： "+userInfoBean.getMsg());
//        System.out.println("昵称: "+userInfoBean.getNickname());
//        System.out.println(userInfoBean.getRet());
//
//        AssessmentPlanBean assessmentPlanBean = new AssessmentPlanBean();
//        assessmentPlanBean.setName(userInfoBean.getNickname());
//        assessmentPlanBean.setSex(userInfoBean.getGender());
//        dao.save(assessmentPlanBean);
//
//        List list = new List();
//        list.add(userInfoBean.getNickname());
//        list.add(userInfoBean.getGender());
//        JsonData jsonData = new JsonData();
//        jsonData.setData((java.util.List) list);
//
//        return jsonData;
//    }

    /**
     * 根据user表中ID添加个人信息
     * @param id
     * @param name
     * @param sex
     * @param email
     * @param telephone
     * @param headportrait
     * @return
     */

    @Override
    public String adduser(Long id, String name, String sex, String email, String telephone, String headportrait) {
        AssessmentPlanBean assessmentPlanBean = new AssessmentPlanBean();
        assessmentPlanBean.setUid(id);
        assessmentPlanBean.setName(name);
        assessmentPlanBean.setSex(sex);
        assessmentPlanBean.setEmail(email);
        assessmentPlanBean.setTelephone(telephone);
        assessmentPlanBean.setHeadportrait(headportrait);
        dao.save(assessmentPlanBean);


        return "添加成功";
    }

    /**
     * 根据学生ID查询学生信息
     * @param code
     * @return
     */
    @Override
    public JsonData getStudentCode(Integer code) {
        String sql = "select id,email from sys_user where code='"+code+"'";
        List<AssessmentPlanBean> list= dao.findByNativeSql(sql, AssessmentPlanBean.class);
        JsonData jsonData = new JsonData();
        jsonData.setData(list);
        return jsonData;
    }

    /**
     * 根据uid查询学生信息
     * @param uid
     * @return
     */
    @Override
    public JsonData getStudentinformation(Long uid) {

        String sql = "select * from mplive_AssessmentPlan where uid='"+uid+"'";
        List<AssessmentPlanBean> list= dao.findByNativeSql(sql, AssessmentPlanBean.class);
        JsonData jsonData = new JsonData();
        jsonData.setData(list);

        return jsonData;
    }

    @Override
    public String updateUserinfo(String name, String sex, String phone, String email, String icon, Long id,Integer code) {
        String message=null;
        String sql = "UPDATE sys_user set name ='"+name+"',sex = '"+sex+"' ,  mobile = '"+phone+"', email = '"+email+"' ," +
                " icon = '"+icon+"',code = '"+code+"' WHERE id ="+id;
        try {
            dao.updateNativeQuery(sql);
            message =  "修改成功";
        } catch (Exception e) {
            message =  "修改失败";
        }
        return message;
    }
}
