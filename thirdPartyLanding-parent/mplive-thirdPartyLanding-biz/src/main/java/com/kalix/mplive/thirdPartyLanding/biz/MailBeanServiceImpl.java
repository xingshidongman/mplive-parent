package com.kalix.mplive.thirdPartyLanding.biz;

import com.kalix.framework.core.util.ConfigUtil;
import com.kalix.mplive.thirdPartyLanding.api.biz.IMailBeanService;
import com.sun.mail.util.MailSSLSocketFactory;

import javax.mail.*;
import javax.mail.internet.*;

import java.util.Properties;
import java.util.Random;

public class MailBeanServiceImpl implements IMailBeanService {
    // IMAP/SMTP服务 密码
    //POP3/SMTP服务
    @Override
    public String getsendMail(String receive) {
        String email = (String) ConfigUtil.getConfigProp("EMAIL","config.mplive.email");
        String password =(String)ConfigUtil.getConfigProp("PASSWORD","config.mplive.email");
        try {

            //设置发件人
            String from = email;

            //设置收件人
            String to = receive;

            //设置邮件发送的服务器，这里为QQ邮件服务器
            String host = "smtp.qq.com";

            //获取系统属性
            Properties properties = System.getProperties();

            //SSL加密
            MailSSLSocketFactory sf = new MailSSLSocketFactory();
            sf.setTrustAllHosts(true);
            properties.put("mail.smtp.ssl.enable", "true");
            properties.put("mail.smtp.ssl.socketFactory", sf);

            //设置系统属性
            properties.setProperty("mail.smtp.host", host);
            properties.put("mail.smtp.auth", "true");

            //获取发送邮件会话、获取第三方登录授权码
            Session session = Session.getDefaultInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(from, password);
                }
            });

            Message message = new MimeMessage(session);

            //防止邮件被当然垃圾邮件处理，披上Outlook的马甲
            message.addHeader("X-Mailer","Microsoft Outlook Express 6.00.2900.2869");

            message.setFrom(new InternetAddress(from));

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            //邮件标题
            message.setSubject("密码找回");

            BodyPart bodyPart = new MimeBodyPart();

            String str="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
            StringBuilder sb=new StringBuilder(4);
            for(int i=0;i<4;i++)
            {
                char ch=str.charAt(new Random().nextInt(str.length()));
                sb.append(ch);
            }

            bodyPart.setText("验证码"+sb);

            Multipart multipart = new MimeMultipart();

            multipart.addBodyPart(bodyPart);

            //附件
//            bodyPart = new MimeBodyPart();
//            String fileName = "D://work/kunlunranqi.zip";
//            DataSource dataSource = new FileDataSource(fileName);
//            bodyPart.setDataHandler(new DataHandler(dataSource));
//            bodyPart.setFileName("文件显示的名称");
//            multipart.addBodyPart(bodyPart);

            message.setContent(multipart);

            Transport.send(message);
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }
}
