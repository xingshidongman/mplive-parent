package com.kalix.mplive.thirdPartyLanding.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.thirdPartyLanding.api.dao.IAssessmentPlanBeanDao;
import com.kalix.mplive.thirdPartyLanding.entities.AssessmentPlanBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 功能描述:
 *
 * @Description: 第三方登陆
 * @Author: whw
 * @CreateDate: 2019/10/23$ 09:16$
 * @Version:
 */
public class AssessmentPlanBeanDaoImpl extends GenericDao<AssessmentPlanBean, Long> implements IAssessmentPlanBeanDao {
    @Override
    @PersistenceContext(unitName = "mplive-thirdPartyLanding-unit")
    public void setEntityManager(EntityManager em) {

        super.setEntityManager(em);
    }
}
