package com.kalix.mplive.school.biz;

import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.framework.core.impl.biz.ShiroGenericBizServiceImpl;
import com.kalix.mplive.school.api.biz.ISchoolBeanService;
import com.kalix.mplive.school.api.dao.ISchoolBeanDao;
import com.kalix.mplive.school.api.dto.SchoolDTO;
import com.kalix.mplive.school.api.dto.SchoolTeacherDTO;
import com.kalix.mplive.school.entities.SchoolBean;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName SchoolBeanServiceImpl
 * @Author ZhaoHang
 * @Date 2019/10/29 9:50
 */
public class SchoolBeanServiceImpl extends ShiroGenericBizServiceImpl<ISchoolBeanDao, SchoolBean> implements ISchoolBeanService {

    /**
     * 获取院系
     * @return
     */
    @Override
    public SchoolDTO getSchool() {
        SchoolDTO schoolDTO = new SchoolDTO();
        schoolDTO.setId((long)-1);
        schoolDTO.setName("根目录");
        String sql = "select s from SchoolBean s";
        List<SchoolBean> list = dao.find(sql);
        List<SchoolDTO> listDTO = new ArrayList<>();
        List<SchoolBean> departmentList = list.stream().filter(sc -> sc.getParentId() == -1).collect(Collectors.toList());
        for (SchoolBean school : departmentList) {
            SchoolDTO dto = new SchoolDTO();
            dto.setId(school.getId());
            dto.setName(school.getName());
            dto.setParentId(school.getParentId());
            dto.setParentName("根目录");
            dto.setCreateBy(school.getCreateBy());
            dto.setCreationDate(school.getCreationDate());
            List<SchoolBean> majorList = list.stream().filter(sc -> sc.getParentId() == school.getId()).collect(Collectors.toList());
            List<SchoolDTO> majorDTOList = new ArrayList<>();
            for (SchoolBean s : majorList) {
                SchoolDTO sdto = new SchoolDTO();
                sdto.setName(s.getName());
                sdto.setId(s.getId());
                sdto.setParentId(s.getParentId());
                sdto.setParentName(school.getName());
                sdto.setCreateBy(s.getCreateBy());
                sdto.setCreationDate(s.getCreationDate());
                majorDTOList.add(sdto);
            }
            dto.setChildren(majorDTOList);
            listDTO.add(dto);
        }
        schoolDTO.setChildren(listDTO);
        return schoolDTO;
    }

    /**
     *    教师申请课程获取院系信息
     * @return
     */
    @Override
    public JsonData getTeacherSchool() {
        String sql = "select s from SchoolBean s";
        List<SchoolBean> list = dao.find(sql);
        List<SchoolTeacherDTO> all =new ArrayList<>();
        JsonData jsonData =new JsonData();
        for(int i=0;i<list.size();i++){
            List<SchoolTeacherDTO> listTeacherDTO = new ArrayList<>();
            SchoolTeacherDTO schoolTeacherDTO =new SchoolTeacherDTO();
            if(list.get(i).getParentId() == -1){
                schoolTeacherDTO.setValue(list.get(i).getId());
                schoolTeacherDTO.setLabel(list.get(i).getName());
                for(int j=0;j<list.size();j++){
                    SchoolTeacherDTO schoolTeacherSunDTO =new SchoolTeacherDTO();
                    if(list.get(j).getParentId() == list.get(i).getId()){
                        schoolTeacherSunDTO.setValue(list.get(j).getId());
                        schoolTeacherSunDTO.setLabel(list.get(j).getName());
                        listTeacherDTO.add(schoolTeacherSunDTO);
                    }
                }
                schoolTeacherDTO.setChildren(listTeacherDTO);
                all.add(schoolTeacherDTO);
            }
        }
        jsonData.setData(all);
        return jsonData;
    }
}
