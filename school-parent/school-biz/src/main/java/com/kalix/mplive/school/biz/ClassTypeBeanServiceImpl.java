package com.kalix.mplive.school.biz;

import com.kalix.framework.core.impl.biz.ShiroGenericBizServiceImpl;
import com.kalix.mplive.school.api.biz.IClassTypeBeanService;
import com.kalix.mplive.school.api.dao.IClassTypeBeanDao;
import com.kalix.mplive.school.entities.ClassTypeBean;

/**
 * @ClassName ClassTypeBeanServiceImpl
 * @Author ZhaoHang
 * @Date 2019/10/29 16:54
 */
public class ClassTypeBeanServiceImpl extends ShiroGenericBizServiceImpl<IClassTypeBeanDao, ClassTypeBean> implements IClassTypeBeanService {
}
