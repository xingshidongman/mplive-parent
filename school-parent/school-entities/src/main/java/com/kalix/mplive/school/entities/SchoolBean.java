package com.kalix.mplive.school.entities;

import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName SchoolBean
 * @Author ZhaoHang
 * @Date 2019/10/28 16:16
 */
@Entity
@Table(name = "mplive_school")
public class SchoolBean extends PersistentEntity  {
    private String name;
    private Integer parentId;
    private String parentName;
    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
}
