package com.kalix.mplive.school.entities;

import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName ClassTypeBean
 * @Author ZhaoHang
 * @Date 2019/10/29 11:51
 */
@Entity
@Table(name = "mplive_classType")
public class ClassTypeBean extends PersistentEntity {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
