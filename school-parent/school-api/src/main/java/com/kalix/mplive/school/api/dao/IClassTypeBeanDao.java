package com.kalix.mplive.school.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.school.entities.ClassTypeBean;

public interface IClassTypeBeanDao  extends IGenericDao<ClassTypeBean, Long> {
}
