package com.kalix.mplive.school.api.dto;

import java.util.List;

/**
 * @ClassName SchoolTeacherDTO
 * @Author ZhaoHang
 * @Date 2019/11/21 11:41
 */
public class SchoolTeacherDTO {
    private long value;
    private String label;
    private List<SchoolTeacherDTO> children;

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<SchoolTeacherDTO> getChildren() {
        return children;
    }

    public void setChildren(List<SchoolTeacherDTO> children) {
        this.children = children;
    }
}
