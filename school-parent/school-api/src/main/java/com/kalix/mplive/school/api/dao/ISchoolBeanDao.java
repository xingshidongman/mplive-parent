package com.kalix.mplive.school.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.school.entities.SchoolBean;

public interface ISchoolBeanDao extends IGenericDao<SchoolBean, Long> {
}
