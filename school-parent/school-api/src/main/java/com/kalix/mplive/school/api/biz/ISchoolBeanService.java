package com.kalix.mplive.school.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.mplive.school.api.dto.SchoolDTO;
import com.kalix.mplive.school.api.dto.SchoolTeacherDTO;
import com.kalix.mplive.school.entities.SchoolBean;


public interface ISchoolBeanService extends IBizService<SchoolBean> {
    SchoolDTO getSchool();
    JsonData getTeacherSchool();

}
