package com.kalix.mplive.school.api.dto;

import com.kalix.framework.core.api.web.model.BaseDTO;
import com.kalix.mplive.school.entities.SchoolBean;

import java.util.List;

/**
 * @ClassName SchoolDTO
 * @Author ZhaoHang
 * @Date 2019/10/30 11:41
 */
public class SchoolDTO extends BaseDTO {
    private String name;
    private Integer parentId;
    private String parentName;
    private List<SchoolDTO> children;

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public List<SchoolDTO> getChildren() {
        return children;
    }

    public void setChildren(List<SchoolDTO> children) {
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

}
