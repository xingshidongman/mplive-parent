package com.kalix.mplive.school.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.mplive.school.entities.ClassTypeBean;

public interface IClassTypeBeanService extends IBizService<ClassTypeBean> {
}
