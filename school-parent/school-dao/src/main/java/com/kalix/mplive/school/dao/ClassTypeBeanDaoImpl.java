package com.kalix.mplive.school.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.school.api.dao.IClassTypeBeanDao;
import com.kalix.mplive.school.entities.ClassTypeBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @ClassName ClassTypeBeanDaoImpl
 * @Author ZhaoHang
 * @Date 2019/10/29 16:52
 */
public class ClassTypeBeanDaoImpl extends GenericDao<ClassTypeBean, Long> implements IClassTypeBeanDao {
    @Override
    @PersistenceContext(unitName = "school-unit")
    public void setEntityManager(EntityManager em) {

        super.setEntityManager(em);
    }
}
