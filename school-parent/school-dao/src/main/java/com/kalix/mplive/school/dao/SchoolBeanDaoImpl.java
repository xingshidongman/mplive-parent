package com.kalix.mplive.school.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.school.api.dao.ISchoolBeanDao;
import com.kalix.mplive.school.entities.SchoolBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @ClassName SchoolBeanDaoImpl
 * @Author ZhaoHang
 * @Date 2019/10/29 9:40
 */
public class SchoolBeanDaoImpl extends GenericDao<SchoolBean, Long> implements ISchoolBeanDao {
    @Override
    @PersistenceContext(unitName = "school-unit")
    public void setEntityManager(EntityManager em) {

        super.setEntityManager(em);
    }
}
