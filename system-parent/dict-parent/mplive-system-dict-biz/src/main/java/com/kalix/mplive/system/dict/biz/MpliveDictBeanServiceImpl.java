package com.kalix.mplive.system.dict.biz;

import com.kalix.framework.core.impl.system.BaseDictServiceImpl;
import com.kalix.mplive.system.dict.api.biz.IMpliveDictBeanService;
import com.kalix.mplive.system.dict.api.dao.IMpliveDictBeanDao;
import com.kalix.mplive.system.dict.entities.MpliveDictBean;

import java.util.ArrayList;
import java.util.List;

public class MpliveDictBeanServiceImpl extends BaseDictServiceImpl<IMpliveDictBeanDao, MpliveDictBean> implements IMpliveDictBeanService {
    /**
     @Override
     public JsonStatus saveEntity(ExamDictBean entity) {
     Integer maxValue = dao.getFieldMaxValue("value", "type='" + entity.getType() + "'");

     maxValue = maxValue + 1;

     entity.setValue(maxValue);

     return super.saveEntity(entity);
     }
     **/
    @Override
    public MpliveDictBean getDictBeanByTypeAndValue(String type, String value) {
        MpliveDictBean mpliveDictBean = new MpliveDictBean();
        String tbName = dao.getTableName();
        String sql = "select * from %s where type='%s' and value='%s'";
        if (tbName != null) {
            sql = String.format(sql, tbName, type, value);
            List list = dao.findByNativeSql(sql, MpliveDictBean.class);
            if (list.size() == 1) {
                mpliveDictBean = (MpliveDictBean) list.get(0);
            }
        }
        return mpliveDictBean;
    }

    @Override
    public List<MpliveDictBean> getDictBeanByType(String type) {
        List<MpliveDictBean> list = new ArrayList<>();
        String tbName = dao.getTableName();
        String sql = "select * from %s where type='%s'";
        if (tbName != null) {
            sql = String.format(sql, tbName, type);
            list = dao.findByNativeSql(sql, MpliveDictBean.class);
        }
        return list;
    }
}
