package com.kalix.mplive.system.dict.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.system.dict.api.dao.IMpliveDictBeanDao;
import com.kalix.mplive.system.dict.entities.MpliveDictBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class MpliveDictBeanDaoImpl extends GenericDao<MpliveDictBean, Long> implements IMpliveDictBeanDao {
    @Override
    @PersistenceContext(unitName = "mplive-system-dict-unit")
    public void setEntityManager(EntityManager em) {
        super.setEntityManager(em);
    }
}
