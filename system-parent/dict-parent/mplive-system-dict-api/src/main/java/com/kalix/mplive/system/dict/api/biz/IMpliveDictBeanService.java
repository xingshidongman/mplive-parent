package com.kalix.mplive.system.dict.api.biz;

import com.kalix.framework.core.api.system.IDictBeanService;
import com.kalix.mplive.system.dict.entities.MpliveDictBean;

import java.util.List;

public interface IMpliveDictBeanService extends IDictBeanService<MpliveDictBean> {
    MpliveDictBean getDictBeanByTypeAndValue(String type, String value);
    List<MpliveDictBean> getDictBeanByType(String type);
}
