package com.kalix.mplive.system.dict.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.system.dict.entities.MpliveDictBean;

public interface IMpliveDictBeanDao extends IGenericDao<MpliveDictBean, Long> {
}
