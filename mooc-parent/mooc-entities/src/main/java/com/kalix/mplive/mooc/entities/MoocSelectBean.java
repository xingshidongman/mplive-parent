package com.kalix.mplive.mooc.entities;

import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName MoocSelectBean
 * @Author ZhaoHang
 * @Date 2019/11/12 15:24
 */
@Entity
@Table(name = "mplive_moocselect")
public class MoocSelectBean extends PersistentEntity {
    private String selectName;
    private long parentId;
    private Boolean use;

    public String getSelectName() {
        return selectName;
    }

    public void setSelectName(String selectName) {
        this.selectName = selectName;
    }

    public Boolean getUse() {
        return use;
    }

    public void setUse(Boolean use) {
        this.use = use;
    }
}
