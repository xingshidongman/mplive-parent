package com.kalix.mplive.mooc.entities;


import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName MoocChapterBean
 * @Author ZhaoHang
 * @Date 2019/10/25 14:27
 */

@Entity
@Table(name = "mplive_moocchapter")
public class MoocChapterBean extends PersistentEntity {
    private long classId;
    private String chapterName;
    private Integer chapterNum;
    private Integer isChapterOpen;
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getChapterNum() {
        return chapterNum;
    }

    public void setChapterNum(Integer chapterNum) {
        this.chapterNum = chapterNum;
    }

    public long getClassId() {
        return classId;
    }

    public void setClassId(long classId) {
        this.classId = classId;
    }

    public Integer getIsChapterOpen() {
        return isChapterOpen;
    }

    public void setIsChapterOpen(Integer isChapterOpen) {
        this.isChapterOpen = isChapterOpen;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }
}
