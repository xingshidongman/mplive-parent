package com.kalix.mplive.mooc.entities;

import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName MoocNoticeBean
 * @Author ZhaoHang
 * @Date 2019/11/26 14:30
 */
@Entity
@Table(name = "mplive_moocnotice")
public class MoocNoticeBean extends PersistentEntity {
    private long personId;
    private String personName;
    private String notice;
    private Boolean isNew;

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public Boolean getNew() {
        return isNew;
    }

    public void setNew(Boolean aNew) {
        isNew = aNew;
    }

    public long getPersonId() {
        return personId;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }
}
