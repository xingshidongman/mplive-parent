package com.kalix.mplive.mooc.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @ClassName MoocApplicationBean
 * @Author ZhaoHang
 * @Date 2019/10/23 17:12
 */
@Entity
@Table(name = "mplive_moocapplication")
public class  MoocApplicationBean extends PersistentEntity {
    private String className;
    @Column(name = "classIntroduce", columnDefinition = "text")
    private String classIntroduce;
    @Column(name = "teacherIntroduce", columnDefinition = "text")
    private String teacherIntroduce;
    @Column(name = "classCover", columnDefinition = "text")
    private String classCover;
    private long teacherId;
    private String teacher;
    private long departmentId;
    private String department;
    private long specialityId;
    private String speciality;
    private long classTypeId;
    private String classType;
    private Date openTime;
    //审核
    private Integer isOpen;
    //是否允许评价
    private Boolean isAppraise;
    //是否推荐
    private Boolean isRecommend;
    //浏览量
    private Integer browseNum;
    //是否收费
    private Boolean charge;
    private Boolean isHot;
    private Integer classOpen;
    private String reason;
    private String judgeOpen;

    public String getJudgeOpen() {
        return judgeOpen;
    }

    public void setJudgeOpen(String judgeOpen) {
        this.judgeOpen = judgeOpen;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getClassOpen() {
        return classOpen;
    }

    public void setClassOpen(Integer classOpen) {
        this.classOpen = classOpen;
    }

    public Boolean getHot() {
        return isHot;
    }

    public void setHot(Boolean hot) {
        isHot = hot;
    }
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public String getTeacher() {
        return teacher;
    }
    public long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(long teacherId) {
        this.teacherId = teacherId;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }
    public Boolean getCharge() {
        return charge;
    }

    public void setCharge(Boolean charge) {
        this.charge = charge;
    }

    public long getClassTypeId() {
        return classTypeId;
    }

    public void setClassTypeId(long classTypeId) {
        this.classTypeId = classTypeId;
    }

    public long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(long departmentId) {
        this.departmentId = departmentId;
    }

    public long getSpecialityId() {
        return specialityId;
    }

    public void setSpecialityId(long specialityId) {
        this.specialityId = specialityId;
    }

    public Integer getBrowseNum() {
        return browseNum;
    }

    public void setBrowseNum(Integer browseNum) {
        this.browseNum = browseNum;
    }

    public Boolean getRecommend() {
        return isRecommend;
    }

    public void setRecommend(Boolean recommend) {
        isRecommend = recommend;
    }

    public Boolean getAppraise() {
        return isAppraise;
    }

    public void setAppraise(Boolean appraise) {
        isAppraise = appraise;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassIntroduce() {
        return classIntroduce;
    }

    public void setClassIntroduce(String classIntroduce) {
        this.classIntroduce = classIntroduce;
    }

    public String getTeacherIntroduce() {
        return teacherIntroduce;
    }

    public void setTeacherIntroduce(String teacherIntroduce) {
        this.teacherIntroduce = teacherIntroduce;
    }

    public String getClassCover() {
        return classCover;
    }

    public void setClassCover(String classCover) {
        this.classCover = classCover;
    }

    public Integer getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(Integer isOpen) {
        this.isOpen = isOpen;
    }
}
