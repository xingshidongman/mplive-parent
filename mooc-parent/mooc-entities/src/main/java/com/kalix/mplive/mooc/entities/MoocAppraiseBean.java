package com.kalix.mplive.mooc.entities;

import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName MoocAppraiseBean
 * @Author ZhaoHang
 * @Date 2019/11/15 17:23
 */
@Entity
@Table(name = "mplive_moocappraise")
public class MoocAppraiseBean extends PersistentEntity {
    @Column(name = "appraise", columnDefinition = "text")
    private String appraise;
    private Integer starLevel;
    private long classId;
    private long studentId;

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public String getAppraise() {
        return appraise;
    }

    public void setAppraise(String appraise) {
        this.appraise = appraise;
    }

    public Integer getStarLevel() {
        return starLevel;
    }

    public void setStarLevel(Integer starLevel) {
        this.starLevel = starLevel;
    }

    public long getClassId() {
        return classId;
    }

    public void setClassId(long classId) {
        this.classId = classId;
    }
}
