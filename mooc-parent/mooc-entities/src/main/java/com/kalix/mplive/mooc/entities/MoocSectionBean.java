package com.kalix.mplive.mooc.entities;

import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName MoocSectionBean
 * @Author ZhaoHang
 * @Date 2019/10/25 15:59
 */

@Entity
@Table(name = "mplive_moocsection")
public class MoocSectionBean extends PersistentEntity {
    private long chapterId;
    private String sectionName;
    private String sectionNum;
    private String classVideo;
    private Integer isSectionOpen;
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getClassVideo() {
        return classVideo;
    }

    public void setClassVideo(String classVideo) {
        this.classVideo = classVideo;
    }

    public String getSectionNum() {
        return sectionNum;
    }

    public void setSectionNum(String sectionNum) {
        this.sectionNum = sectionNum;
    }

    public long getChapterId() {
        return chapterId;
    }

    public void setChapterId(long chapterId) {
        this.chapterId = chapterId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public Integer getIsSectionOpen() {
        return isSectionOpen;
    }

    public void setIsSectionOpen(Integer isSectionOpen) {
        this.isSectionOpen = isSectionOpen;
    }
}
