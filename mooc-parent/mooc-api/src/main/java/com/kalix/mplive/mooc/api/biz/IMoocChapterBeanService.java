package com.kalix.mplive.mooc.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.mplive.mooc.entities.MoocChapterBean;

import java.util.List;

public interface IMoocChapterBeanService extends IBizService<MoocChapterBean> {
    JsonData getValue(long id);
    void storeChapter(String lessonData);
    void deleteOldChapter(String list);
}
