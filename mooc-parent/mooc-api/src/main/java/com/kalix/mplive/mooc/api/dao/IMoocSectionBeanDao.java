package com.kalix.mplive.mooc.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.mooc.entities.MoocSectionBean;

public interface IMoocSectionBeanDao extends IGenericDao<MoocSectionBean,Long> {
}
