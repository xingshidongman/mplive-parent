package com.kalix.mplive.mooc.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.mplive.mooc.entities.MoocNoticeBean;

public interface IMoocNoticeBeanService extends IBizService<MoocNoticeBean> {
    JsonData getTeacherNotice(long teacherId,String teacher,int page);
    JsonData getStudentNotice(long studentId,int page);
    Integer getNewNoticeNum(long id);
    JsonStatus liveNotice(long classId, String studentNotice);
}
