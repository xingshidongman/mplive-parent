package com.kalix.mplive.mooc.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.mplive.mooc.entities.MoocAppraiseBean;


public interface IMoocAppraiseBeanService extends IBizService<MoocAppraiseBean> {
    JsonData getTeacherAppraise(long teacherId,int page);
    JsonData getClassAppraise(long classId,int page);
    JsonData getAppraiseByUser(long classId,long userId);
}
