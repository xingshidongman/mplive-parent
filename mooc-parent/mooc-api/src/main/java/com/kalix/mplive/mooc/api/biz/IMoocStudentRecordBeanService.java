package com.kalix.mplive.mooc.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.mplive.mooc.entities.MoocStudentRecordBean;

public interface IMoocStudentRecordBeanService extends IBizService<MoocStudentRecordBean> {
    JsonData getRecord(long studentId);
    JsonData judgeIsExist(long studentId, long classId);
}
