package com.kalix.mplive.mooc.api.dto;

import java.util.Date;

/**
 * @ClassName MoocGetAllDTO
 * @Author ZhaoHang
 * @Date 2019/11/22 8:49
 */
public class MoocGetAllDTO {
    private String className;
    private String classIntroduce;
    private String teacherIntroduce;
    private String classCover;
    private String teacher;
    private String department;
    private String speciality;
    private String classType;
    private Date openTime;
    //是否开课
    private Integer isOpen;
    //是否允许评价
    private Boolean isAppraise;
    //是否推荐
    private Boolean isRecommend;
    //浏览量
    private Integer browseNum;
    //是否收费
    private Boolean charge;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassIntroduce() {
        return classIntroduce;
    }

    public void setClassIntroduce(String classIntroduce) {
        this.classIntroduce = classIntroduce;
    }

    public String getTeacherIntroduce() {
        return teacherIntroduce;
    }

    public void setTeacherIntroduce(String teacherIntroduce) {
        this.teacherIntroduce = teacherIntroduce;
    }

    public String getClassCover() {
        return classCover;
    }

    public void setClassCover(String classCover) {
        this.classCover = classCover;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Integer getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(Integer isOpen) {
        this.isOpen = isOpen;
    }

    public Boolean getAppraise() {
        return isAppraise;
    }

    public void setAppraise(Boolean appraise) {
        isAppraise = appraise;
    }

    public Boolean getRecommend() {
        return isRecommend;
    }

    public void setRecommend(Boolean recommend) {
        isRecommend = recommend;
    }

    public Integer getBrowseNum() {
        return browseNum;
    }

    public void setBrowseNum(Integer browseNum) {
        this.browseNum = browseNum;
    }

    public Boolean getCharge() {
        return charge;
    }

    public void setCharge(Boolean charge) {
        this.charge = charge;
    }
}
