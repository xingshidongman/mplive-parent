package com.kalix.mplive.mooc.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.mplive.mooc.api.dto.MoocApplicationDTO;
import com.kalix.mplive.mooc.api.dto.MoocDetailsDTO;
import com.kalix.mplive.mooc.entities.MoocApplicationBean;
import java.lang.String;

public interface IMoocApplicationBeanService extends IBizService<MoocApplicationBean> {
    MoocApplicationBean getSubmit(String className, String classIntroduce, String teacher, String teacherIntroduce, String classCover, long teacherId,
                   long departmentId, long specialityId, long classTypeId, String openTime,Boolean isAppraise,Boolean charge,int classOpen);
    JsonData getRecommend();
    MoocApplicationDTO getClassInChapter();
    MoocApplicationDTO getExamineChapter();
    JsonData getMoocClass(long departmentId, long specialityId, long classTypeId, String date,int moocPage);
    void plusBrowseNum(long id,int browseNum);
    MoocDetailsDTO getClassDetails(long classId);
    JsonData getTeacherClass(long teacherId);
    JsonData fuzzySearch(String word,int page);
    JsonData getReason(long classId);
    JsonData getAllMoocClass();
}
