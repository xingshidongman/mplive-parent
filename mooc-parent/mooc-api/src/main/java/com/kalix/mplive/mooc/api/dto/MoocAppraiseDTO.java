package com.kalix.mplive.mooc.api.dto;

/**
 * @ClassName MoocAppraiseDTO
 * @Author ZhaoHang
 * @Date 2019/11/15 17:30
 */
public class MoocAppraiseDTO {
    private long classId;
    private String appraise;
    private Integer starLevel;
    private String className;
    private String studentName;
    private String headportrait;
    private String classcover;

    public String getHeadportrait() {
        return headportrait;
    }

    public void setHeadportrait(String headportrait) {
        this.headportrait = headportrait;
    }

    public String getClasscover() {
        return classcover;
    }

    public void setClasscover(String classcover) {
        this.classcover = classcover;
    }

    public long getClassId() {
        return classId;
    }

    public void setClassId(long classId) {
        this.classId = classId;
    }

    public String getAppraise() {
        return appraise;
    }

    public void setAppraise(String appraise) {
        this.appraise = appraise;
    }

    public Integer getStarLevel() {
        return starLevel;
    }

    public void setStarLevel(Integer starLevel) {
        this.starLevel = starLevel;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
}
