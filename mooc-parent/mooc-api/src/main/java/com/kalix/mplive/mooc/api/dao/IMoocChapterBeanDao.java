package com.kalix.mplive.mooc.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.mooc.entities.MoocChapterBean;

public interface IMoocChapterBeanDao extends IGenericDao<MoocChapterBean,Long> {
}
