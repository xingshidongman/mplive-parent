package com.kalix.mplive.mooc.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.mooc.entities.MoocNoticeBean;

public interface IMoocNoticeBeanDao extends IGenericDao<MoocNoticeBean,Long> {
}
