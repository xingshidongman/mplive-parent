package com.kalix.mplive.mooc.api.dto;

import com.kalix.mplive.mooc.entities.MoocSectionBean;

import java.util.List;

/**
 * @ClassName MoocChapterDTO
 * @Author ZhaoHang
 * @Date 2019/10/25 16:55
 */
public class MoocChapterDTO {
    private String chapterName;
    private Integer chapterNum;
    private Integer isChapterOpen;
    private List<MoocSectionBean> children;

    public Integer getIsChapterOpen() {
        return isChapterOpen;
    }

    public void setIsChapterOpen(Integer isChapterOpen) {
        this.isChapterOpen = isChapterOpen;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public Integer getChapterNum() {
        return chapterNum;
    }

    public void setChapterNum(Integer chapterNum) {
        this.chapterNum = chapterNum;
    }

    public List<MoocSectionBean> getChildren() {
        return children;
    }

    public void setChildren(List<MoocSectionBean> children) {
        this.children = children;
    }
}
