package com.kalix.mplive.mooc.api.dto;

/**
 * @ClassName SelectBeanDTO
 * @Author ZhaoHang
 * @Date 2019/12/3 14:59
 */
public class SelectBeanDTO {
    private String name;
    private long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
