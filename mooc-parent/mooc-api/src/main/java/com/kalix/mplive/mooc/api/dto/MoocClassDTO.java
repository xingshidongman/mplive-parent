package com.kalix.mplive.mooc.api.dto;

/**
 * @ClassName MoocClassDTO
 * @Author ZhaoHang
 * @Date 2019/10/29 11:41
 */
public class MoocClassDTO {
    private String className;
    private String classIntroduce;
    private String teacherIntroduce;
    private String classCover;
    private String department;
    private String speciality;
    private String classType;
    private long teacherName;
    //是否开课
    private Boolean isOpen;
    //是否允许评价
    private Boolean isAppraise;
    //是否推荐
    private Boolean isRecommend;
    //浏览量
    private Integer browseNum;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassIntroduce() {
        return classIntroduce;
    }

    public void setClassIntroduce(String classIntroduce) {
        this.classIntroduce = classIntroduce;
    }

    public String getTeacherIntroduce() {
        return teacherIntroduce;
    }

    public void setTeacherIntroduce(String teacherIntroduce) {
        this.teacherIntroduce = teacherIntroduce;
    }

    public String getClassCover() {
        return classCover;
    }

    public void setClassCover(String classCover) {
        this.classCover = classCover;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public long getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(long teacherName) {
        this.teacherName = teacherName;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public Boolean getAppraise() {
        return isAppraise;
    }

    public void setAppraise(Boolean appraise) {
        isAppraise = appraise;
    }

    public Boolean getRecommend() {
        return isRecommend;
    }

    public void setRecommend(Boolean recommend) {
        isRecommend = recommend;
    }

    public Integer getBrowseNum() {
        return browseNum;
    }

    public void setBrowseNum(Integer browseNum) {
        this.browseNum = browseNum;
    }
}
