package com.kalix.mplive.mooc.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.mooc.entities.MoocStudentRecordBean;

public interface IMoocStudentRecordBeanDao extends IGenericDao<MoocStudentRecordBean,Long> {
}
