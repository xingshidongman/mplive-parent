package com.kalix.mplive.mooc.api.dto;

/**
 * @ClassName MoocSchoolDTO
 * @Author ZhaoHang
 * @Date 2019/11/22 11:07
 */
public class MoocSchoolDTO {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
