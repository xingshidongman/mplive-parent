package com.kalix.mplive.mooc.api.dto;

/**
 * @ClassName MoocReasonDTO
 * @Author ZhaoHang
 * @Date 2019/12/11 9:23
 */
public class MoocReasonDTO {
    private long id;
    private float num;
    private String name;
    private String reason;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getNum() {
        return num;
    }

    public void setNum(float num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
