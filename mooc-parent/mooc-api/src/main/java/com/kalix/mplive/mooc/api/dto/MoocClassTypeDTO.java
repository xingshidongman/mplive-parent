package com.kalix.mplive.mooc.api.dto;

/**
 * @ClassName MoocClassTypeDTO
 * @Author ZhaoHang
 * @Date 2019/11/22 10:56
 */
public class MoocClassTypeDTO {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
