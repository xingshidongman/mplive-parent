package com.kalix.mplive.mooc.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.mplive.mooc.entities.MoocSectionBean;

public interface IMoocSectionBeanService extends IBizService<MoocSectionBean> {
    JsonData getValue(long id);
}
