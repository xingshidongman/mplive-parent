package com.kalix.mplive.mooc.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.mooc.entities.MoocApplicationBean;

public interface IMoocApplicationBeanDao extends IGenericDao<MoocApplicationBean,Long> {
}
