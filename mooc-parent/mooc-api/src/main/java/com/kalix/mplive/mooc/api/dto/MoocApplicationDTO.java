package com.kalix.mplive.mooc.api.dto;

import java.util.List;

/**
 * @ClassName MoocApplicationDTO
 * @Author ZhaoHang
 * @Date 2019/11/6 11:40
 */
public class MoocApplicationDTO {
    private long id;
    private String text;
    private List<MoocApplicationDTO> children;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<MoocApplicationDTO> getChildren() {
        return children;
    }

    public void setChildren(List<MoocApplicationDTO> children) {
        this.children = children;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
