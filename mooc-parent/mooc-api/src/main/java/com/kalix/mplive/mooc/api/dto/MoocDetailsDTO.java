package com.kalix.mplive.mooc.api.dto;

import com.kalix.mplive.mooc.entities.MoocSectionBean;

import java.util.Date;
import java.util.List;

/**
 * @ClassName MoocDetailsDTO
 * @Author ZhaoHang
 * @Date 2019/12/4 9:51
 */
public class MoocDetailsDTO {
    private List<MoocChapterDTO> list;
    private String className;
    private String teacher;
    private String classIntroduce;
    private String teacherIntroduce;
    private String icon;
    private String classCover;
    private String department;
    private String speciality;
    private String classType;
    private long departmentId;
    private long specialityId;
    private long classTypeId;
    private Date openTime;
    //是否开课
    private Integer isOpen;
    //是否允许评价
    private Boolean isAppraise;
    //是否推荐
    private Boolean isRecommend;
    //浏览量
    private Integer browseNum;
    //是否收费
    private Boolean charge;

    public long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(long departmentId) {
        this.departmentId = departmentId;
    }

    public long getSpecialityId() {
        return specialityId;
    }

    public void setSpecialityId(long specialityId) {
        this.specialityId = specialityId;
    }

    public long getClassTypeId() {
        return classTypeId;
    }

    public void setClassTypeId(long classTypeId) {
        this.classTypeId = classTypeId;
    }

    public String getClassCover() {
        return classCover;
    }

    public void setClassCover(String classCover) {
        this.classCover = classCover;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Integer getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(Integer isOpen) {
        this.isOpen = isOpen;
    }

    public Boolean getAppraise() {
        return isAppraise;
    }

    public void setAppraise(Boolean appraise) {
        isAppraise = appraise;
    }

    public Boolean getRecommend() {
        return isRecommend;
    }

    public void setRecommend(Boolean recommend) {
        isRecommend = recommend;
    }

    public Integer getBrowseNum() {
        return browseNum;
    }

    public void setBrowseNum(Integer browseNum) {
        this.browseNum = browseNum;
    }

    public Boolean getCharge() {
        return charge;
    }

    public void setCharge(Boolean charge) {
        this.charge = charge;
    }

    public List<MoocChapterDTO> getList() {
        return list;
    }

    public void setList(List<MoocChapterDTO> list) {
        this.list = list;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getClassIntroduce() {
        return classIntroduce;
    }

    public void setClassIntroduce(String classIntroduce) {
        this.classIntroduce = classIntroduce;
    }

    public String getTeacherIntroduce() {
        return teacherIntroduce;
    }

    public void setTeacherIntroduce(String teacherIntroduce) {
        this.teacherIntroduce = teacherIntroduce;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
