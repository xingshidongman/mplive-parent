package com.kalix.mplive.mooc.api.dto;

import java.util.List;

/**
 * @ClassName SelectDTO
 * @Author ZhaoHang
 * @Date 2019/11/13 17:06
 */
public class SelectDTO {
    private String title;
    private List<SelectBeanDTO> listDto;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SelectBeanDTO> getListDto() {
        return listDto;
    }

    public void setListDto(List<SelectBeanDTO> listDto) {
        this.listDto = listDto;
    }
}
