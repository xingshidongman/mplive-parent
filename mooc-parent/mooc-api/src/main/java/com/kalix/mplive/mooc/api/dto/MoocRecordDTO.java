package com.kalix.mplive.mooc.api.dto;

import java.util.Date;

/**
 * @ClassName MoocRecordDTO
 * @Author ZhaoHang
 * @Date 2019/12/20 16:27
 */
public class MoocRecordDTO {
    private long id;
    private long classId;
    private String className;
    private Date inTime;
    private String classCover;

    public long getClassId() {
        return classId;
    }

    public void setClassId(long classId) {
        this.classId = classId;
    }

    public String getClassCover() {
        return classCover;
    }

    public void setClassCover(String classCover) {
        this.classCover = classCover;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Date getInTime() {
        return inTime;
    }

    public void setInTime(Date inTime) {
        this.inTime = inTime;
    }
}
