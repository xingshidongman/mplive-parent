package com.kalix.mplive.mooc.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.mooc.api.dao.IMoocChapterBeanDao;
import com.kalix.mplive.mooc.entities.MoocChapterBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @ClassName MoocChapterBeanDaoImpl
 * @Author ZhaoHang
 * @Date 2019/10/28 11:36
 */
public class MoocChapterBeanDaoImpl extends GenericDao<MoocChapterBean, Long> implements IMoocChapterBeanDao {
    @Override
    @PersistenceContext(unitName = "mooc-unit")
    public void setEntityManager(EntityManager em) {
        super.setEntityManager(em);
    }
}
