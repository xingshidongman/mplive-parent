package com.kalix.mplive.mooc.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.mooc.api.dao.IMoocSectionBeanDao;
import com.kalix.mplive.mooc.entities.MoocSectionBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @ClassName MoocSectionBeanDaoImpl
 * @Author ZhaoHang
 * @Date 2019/10/28 11:59
 */
public class MoocSectionBeanDaoImpl extends GenericDao<MoocSectionBean, Long> implements IMoocSectionBeanDao {
    @Override
    @PersistenceContext(unitName = "mooc-unit")
    public void setEntityManager(EntityManager em) {
        super.setEntityManager(em);
    }
}
