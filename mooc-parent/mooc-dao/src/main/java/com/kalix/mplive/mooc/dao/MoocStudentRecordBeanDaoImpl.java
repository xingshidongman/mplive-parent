package com.kalix.mplive.mooc.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.mooc.api.dao.IMoocStudentRecordBeanDao;
import com.kalix.mplive.mooc.entities.MoocStudentRecordBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @ClassName MoocStudentRecordBeanDaoImpl
 * @Author ZhaoHang
 * @Date 2019/12/6 9:36
 */
public class MoocStudentRecordBeanDaoImpl extends GenericDao<MoocStudentRecordBean, Long> implements IMoocStudentRecordBeanDao {
    @Override
    @PersistenceContext(unitName = "mooc-unit")
    public void setEntityManager(EntityManager em) {
        super.setEntityManager(em);
    }
}
