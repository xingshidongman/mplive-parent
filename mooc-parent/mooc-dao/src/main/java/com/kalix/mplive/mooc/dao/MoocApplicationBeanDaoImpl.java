package com.kalix.mplive.mooc.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.mooc.api.dao.IMoocApplicationBeanDao;
import com.kalix.mplive.mooc.entities.MoocApplicationBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @ClassName MoocApplicationBeanDaoImpl
 * @Author ZhaoHang
 * @Date 2019/10/24 10:33
 */
public class MoocApplicationBeanDaoImpl extends GenericDao<MoocApplicationBean, Long> implements IMoocApplicationBeanDao {
    @Override
    @PersistenceContext(unitName = "mooc-unit")
    public void setEntityManager(EntityManager em) {
        super.setEntityManager(em);
    }
}
