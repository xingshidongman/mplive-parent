package com.kalix.mplive.mooc.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.mooc.api.dao.IMoocNoticeBeanDao;
import com.kalix.mplive.mooc.entities.MoocNoticeBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @ClassName MoocNoticeBeanDaoImpl
 * @Author ZhaoHang
 * @Date 2019/11/26 15:04
 */
public class MoocNoticeBeanDaoImpl extends GenericDao<MoocNoticeBean, Long> implements IMoocNoticeBeanDao {
    @Override
    @PersistenceContext(unitName = "mooc-unit")
    public void setEntityManager(EntityManager em) {
        super.setEntityManager(em);
    }
}
