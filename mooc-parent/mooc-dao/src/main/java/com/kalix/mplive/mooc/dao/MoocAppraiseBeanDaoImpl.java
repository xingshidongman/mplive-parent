package com.kalix.mplive.mooc.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.mooc.api.dao.IMoocAppraiseBeanDao;
import com.kalix.mplive.mooc.entities.MoocAppraiseBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @ClassName MoocAppraiseBeanDaoImpl
 * @Author ZhaoHang
 * @Date 2019/11/18 9:14
 */
public class MoocAppraiseBeanDaoImpl extends GenericDao<MoocAppraiseBean, Long> implements IMoocAppraiseBeanDao {
    @Override
    @PersistenceContext(unitName = "mooc-unit")
    public void setEntityManager(EntityManager em) {
        super.setEntityManager(em);
    }
}
