package com.kalix.mplive.mooc.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.mooc.api.dao.IMoocSelectBeanDao;
import com.kalix.mplive.mooc.entities.MoocSelectBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @ClassName MoocSelectBeanDaoImpl
 * @Author ZhaoHang
 * @Date 2019/11/12 15:28
 */
public class MoocSelectBeanDaoImpl extends GenericDao<MoocSelectBean, Long> implements IMoocSelectBeanDao {
    @Override
    @PersistenceContext(unitName = "mooc-unit")
    public void setEntityManager(EntityManager em) {
        super.setEntityManager(em);
    }
}
