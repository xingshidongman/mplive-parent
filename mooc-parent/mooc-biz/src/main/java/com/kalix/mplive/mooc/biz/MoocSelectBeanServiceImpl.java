package com.kalix.mplive.mooc.biz;

import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.impl.biz.GenericBizServiceImpl;
import com.kalix.mplive.mooc.api.biz.IMoocSelectBeanService;
import com.kalix.mplive.mooc.api.dao.IMoocSelectBeanDao;
import com.kalix.mplive.mooc.api.dto.SelectBeanDTO;
import com.kalix.mplive.mooc.api.dto.SelectDTO;
import com.kalix.mplive.mooc.entities.MoocSelectBean;
import com.kalix.mplive.school.api.dao.IClassTypeBeanDao;
import com.kalix.mplive.school.api.dao.ISchoolBeanDao;
import com.kalix.mplive.school.entities.ClassTypeBean;
import com.kalix.mplive.school.entities.SchoolBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName MoocSelectBeanServiceImpl
 * @Author ZhaoHang
 * @Date 2019/11/12 15:31
 */
public class MoocSelectBeanServiceImpl extends GenericBizServiceImpl<IMoocSelectBeanDao, MoocSelectBean> implements IMoocSelectBeanService {
    private ISchoolBeanDao schoolDao;

    public void setSchoolDao(ISchoolBeanDao schoolDao) {
        this.schoolDao = schoolDao;
    }
    private IClassTypeBeanDao classTypeDao;

    public void setClassTypeDao(IClassTypeBeanDao classTypeDao) {
        this.classTypeDao = classTypeDao;
    }

    /**
     * 获取学生端所有筛选条件
     * @return
     */
    @Override
    public JsonData getSelect() {
        JsonData jsonData = new JsonData();
        String sql="select * from mplive_moocselect m where m.use = true";
        List<MoocSelectBean> list = dao.findByNativeSql(sql,MoocSelectBean.class );
        List<SelectDTO> listAll = new ArrayList<>();
        for(int i=0;i<list.size();i++){
            SelectDTO selectDto= new SelectDTO();
            if(list.get(i).getSelectName().equals("所属院系")){
                String sqlDepartment="select * from mplive_school where parentid= -1";
                List <SchoolBean> listDepartment = schoolDao.findByNativeSql(sqlDepartment, SchoolBean.class);
                selectDto.setTitle(list.get(i).getSelectName());
                List<SelectBeanDTO> stringList = new ArrayList<>();
                for(int j=0;j<listDepartment.size();j++){
                    SelectBeanDTO selectBeanDTO = new SelectBeanDTO();
                    selectBeanDTO.setId(listDepartment.get(j).getId());
                    selectBeanDTO.setName(listDepartment.get(j).getName());
                    stringList.add(selectBeanDTO);
                }
                selectDto.setListDto(stringList);
                listAll.add(selectDto);
            }
            if(list.get(i).getSelectName().equals("所属专业")){
                String sqlSpeciality="select * from mplive_school  where parentid != -1";
                List <SchoolBean> listDepartment = schoolDao.findByNativeSql(sqlSpeciality, SchoolBean.class);
                selectDto.setTitle(list.get(i).getSelectName());
                List<SelectBeanDTO> stringList = new ArrayList<>();
                for(int j=0;j<listDepartment.size();j++){
                    SelectBeanDTO selectBeanDTO = new SelectBeanDTO();
                    selectBeanDTO.setId(listDepartment.get(j).getId());
                    selectBeanDTO.setName(listDepartment.get(j).getName());
                    stringList.add(selectBeanDTO);
                }
                selectDto.setListDto(stringList);
                listAll.add(selectDto);
            }
            if(list.get(i).getSelectName().equals("课程类别")){
                String sqlClassType="select * from mplive_classType";
                List <ClassTypeBean> listDepartment = classTypeDao.findByNativeSql(sqlClassType, ClassTypeBean.class);
                selectDto.setTitle(list.get(i).getSelectName());
                List<SelectBeanDTO> stringList = new ArrayList<>();
                for(int j=0;j<listDepartment.size();j++){
                    SelectBeanDTO selectBeanDTO = new SelectBeanDTO();
                    selectBeanDTO.setId(listDepartment.get(j).getId());
                    selectBeanDTO.setName(listDepartment.get(j).getName());
                    stringList.add(selectBeanDTO);
                }
                selectDto.setListDto(stringList);
                listAll.add(selectDto);
            }
            if(list.get(i).getSelectName().equals("课程年份")){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
                Date date = new Date();
                int now= Integer.valueOf(sdf.format(date));
                List<SelectBeanDTO> listDate = new ArrayList<>();
                for(int j=0;j<5;j++){
                    SelectBeanDTO selectBeanDTO = new SelectBeanDTO();
                    selectBeanDTO.setName(String.valueOf(now-j));
                    listDate.add(selectBeanDTO);
                }
                selectDto.setTitle(list.get(i).getSelectName());
                selectDto.setListDto(listDate);
                listAll.add(selectDto);
            }
        }
        jsonData.setData(listAll);
        return jsonData;
    }
}
