package com.kalix.mplive.mooc.biz;

import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.impl.biz.GenericBizServiceImpl;
import com.kalix.mplive.mooc.api.biz.IMoocAppraiseBeanService;
import com.kalix.mplive.mooc.api.dao.IMoocAppraiseBeanDao;
import com.kalix.mplive.mooc.api.dto.MoocAppraiseDTO;
import com.kalix.mplive.mooc.entities.MoocApplicationBean;
import com.kalix.mplive.mooc.entities.MoocAppraiseBean;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName MoocAppraiseBeanServiceImpl
 * @Author ZhaoHang
 * @Date 2019/11/18 9:15
 */
public class MoocAppraiseBeanServiceImpl extends GenericBizServiceImpl<IMoocAppraiseBeanDao, MoocAppraiseBean> implements IMoocAppraiseBeanService {
    /**
     * 获取老师所有慕课的评价
     * @param teacherId
     * @param page
     * @return
     */
    @Override
    public JsonData getTeacherAppraise(long teacherId,int page) {
        JsonData jsonData = new JsonData();
        String sql = "SELECT" +
                " M . ID AS classId," +
                "M .classcover," +
                "ma.appraise," +
                "ma.starLevel," +
                "M .classcover," +
                "su. NAME AS studentName," +
                "su.icon as headportrait" +
                " FROM" +
                " mplive_moocappraise ma," +
                "mplive_moocapplication M," +
                "sys_user su" +
                " WHERE" +
                " M . ID = ma.classid" +
                " AND M .teacherId = '"+teacherId+"'" +
                " and ma.studentid = su.id" +
                " ORDER BY" +
                " ma.creationdate DESC";
        List<MoocAppraiseDTO> list =dao.findByNativeSql(sql,MoocAppraiseDTO.class);
        jsonData.setTotalCount((long)list.size());
        if(list.size()>0){
            int numPage = 0;
            if (list.size() % 6 == 0) {
                numPage = list.size() / 6;
            } else {
                numPage = list.size() / 6 + 1;
            }
            List<List<MoocAppraiseDTO>> listPge = new ArrayList<>();
            for (int i = 0; i < numPage; i++) {
                List<MoocAppraiseDTO> temp = new ArrayList<>();
                for (int j = i * 6; j < (i + 1) * 6; j++) {
                    if (j==list.size()) {
                        break;
                    }
                    temp.add(list.get(j));
                }
                listPge.add(temp);
            }
            jsonData.setData(listPge.get(page-1));
        }else{
            jsonData.setData(list);
        }
        return jsonData;
    }

    /**
     * 获取课程下的所有评价
     * @param classId
     * @param page
     * @return
     */
    @Override
    public JsonData getClassAppraise(long classId, int page) {
//        int max = page*6;
//        int min = (page-1)*6;
        JsonData jsonData = new JsonData();
        String sql = "SELECT ma.appraise,ma.starLevel,u.name as studentName" +
                " FROM mplive_moocappraise ma,sys_user u " +
                " WHERE ma.classId = "+classId+" and ma.studentid = u.id" +
                " ORDER BY ma.creationdate DESC ";
        List<MoocAppraiseDTO> list =dao.findByNativeSql(sql,MoocAppraiseDTO.class);
        jsonData.setTotalCount((long)list.size());
        if(list.size()>0){
            int numPage = 0;
            if (list.size() % 6 == 0) {
                numPage = list.size() / 6;
            } else {
                numPage = list.size() / 6 + 1;
            }
            List<List<MoocAppraiseDTO>> listPge = new ArrayList<>();
            for (int i = 0; i < numPage; i++) {
                List<MoocAppraiseDTO> temp = new ArrayList<>();
                for (int j = i * 6; j < (i + 1) * 6; j++) {
                    if (j==list.size()) {
                        break;
                    }
                    temp.add(list.get(j));
                }
                listPge.add(temp);
            }
            jsonData.setData(listPge.get(page-1));
        }else{
            jsonData.setData(list);
        }
        return jsonData;
    }

    /**
     * 查看用户在慕课课程下是否有评论
     * @param classId
     * @param userId
     * @return
     */
    @Override
    public JsonData getAppraiseByUser(long classId, long userId) {
        String sql = "select * from mplive_moocappraise where classId = "+classId+" and studentId = "+userId;
        List<MoocAppraiseBean> list = dao.findByNativeSql(sql, MoocAppraiseBean.class);
        JsonData jsonData = new JsonData();
        jsonData.setData(list);
        return jsonData;
    }
}
