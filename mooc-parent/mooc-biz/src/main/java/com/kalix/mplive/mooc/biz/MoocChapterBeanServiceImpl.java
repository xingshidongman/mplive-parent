package com.kalix.mplive.mooc.biz;

import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.impl.biz.GenericBizServiceImpl;
import com.kalix.mplive.mooc.api.biz.IMoocChapterBeanService;
import com.kalix.mplive.mooc.api.dao.IMoocChapterBeanDao;
import com.kalix.mplive.mooc.api.dao.IMoocSectionBeanDao;
import com.kalix.mplive.mooc.entities.MoocChapterBean;
import com.kalix.mplive.mooc.entities.MoocSectionBean;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName MoocChapterBeanServiceImpl
 * @Author ZhaoHang
 * @Date 2019/10/28 11:46
 */
public class MoocChapterBeanServiceImpl extends GenericBizServiceImpl<IMoocChapterBeanDao, MoocChapterBean> implements IMoocChapterBeanService {
    private IMoocSectionBeanDao sectionDao;

    public void setSectionDao(IMoocSectionBeanDao sectionDao) {
        this.sectionDao = sectionDao;
    }

    /**
     *按照章数升序获取章
     * @param id
     * @return
     */
    @Override
    public JsonData getValue(long id) {
        String sql = "select a from MoocChapterBean a where a.classId = " + id + "order by a.chapterNum";
        List<MoocChapterBean> list = dao.find(sql);
        JsonData jsonData = new JsonData();
        jsonData.setData(list);
        jsonData.setTotalCount((long) list.size());
        return jsonData;
    }

    /**
     * 课程申请添加章节信息
     * @param lessonData    章节信息
     */
    @Override
    public void storeChapter(String lessonData) {
        JSONObject j = new JSONObject(lessonData);
        JSONArray jsonArray = j.getJSONArray("name");
        long chapterId = 0;
        int chapterNum = 0;
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = new JSONObject(jsonArray.get(i).toString());
            JSONArray chapterArray = jsonObject.getJSONArray("chapter");//章
            JSONArray sectionArray = jsonObject.getJSONArray("section");//节
            String classVideo = jsonObject.getString("lesson");//类似1.01,1.02
            if (!chapterArray.get(0).equals(null)) {
                MoocChapterBean chapterBean = new MoocChapterBean();
                chapterBean.setChapterNum(Integer.valueOf(chapterArray.get(0).toString()));// chapterArray.get(0).toString() 取章节
                chapterBean.setChapterName(String.valueOf(chapterArray.get(1)));// chapterArray.get(1) 取章节名称
                chapterBean.setClassId((long) Integer.valueOf(chapterArray.get(2).toString()));
                chapterBean.setIsChapterOpen(0);
                chapterId = dao.save(chapterBean).getId();
                chapterNum = dao.save(chapterBean).getChapterNum();
            }
            if (Math.floor((Float.parseFloat(sectionArray.get(0).toString()))) == chapterNum) { // sectionArray.get(0).toString()取章节类似1.01,1.02
                MoocSectionBean sectionBean = new MoocSectionBean();
                sectionBean.setChapterId(chapterId);
                sectionBean.setIsSectionOpen(0);
                sectionBean.setSectionName(String.valueOf(sectionArray.get(1)));
                sectionBean.setSectionNum(sectionArray.get(0).toString());
                sectionBean.setClassVideo(classVideo);
                sectionDao.save(sectionBean);
            }
        }
    }

    /**
     * 修改章节后删除之前的章节
     * @param list
     */
    @Override
    public void deleteOldChapter(String list) {
        if(list!=null&&!list.equals("")){
            String[] listStr = list.split(",");
            List<String> listAll= Arrays.asList(listStr);
            for(int i = 0;i<listAll.size();i++){
                dao.remove(Long.parseLong(listAll.get(i)));
                String deleteSectionSql = "delete from mplive_moocsection where chapterId = "+Long.parseLong(listAll.get(i));
                dao.updateNativeQuery(deleteSectionSql);
            }
        }
    }


}
