package com.kalix.mplive.mooc.biz;

import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.impl.biz.GenericBizServiceImpl;
import com.kalix.mplive.mooc.api.biz.IMoocSectionBeanService;
import com.kalix.mplive.mooc.api.dao.IMoocSectionBeanDao;
import com.kalix.mplive.mooc.entities.MoocSectionBean;

import java.util.List;

/**
 * @ClassName MoocSectionBeanServiceImpl
 * @Author ZhaoHang
 * @Date 2019/10/28 12:02
 */
public class MoocSectionBeanServiceImpl extends GenericBizServiceImpl<IMoocSectionBeanDao, MoocSectionBean> implements IMoocSectionBeanService {
    /**
     * 获取章id下的所有节信息
     * @param id
     * @return
     */
    @Override
    public JsonData getValue(long id) {
        String sql = "select a from MoocSectionBean a where a.chapterId = "+id +" order by a.sectionNum";
        List<MoocSectionBean> list = dao.find(sql);
        JsonData jsonData =new JsonData();
        jsonData.setData(list);
        jsonData.setTotalCount((long)list.size());
        return jsonData;
    }
}
