package com.kalix.mplive.mooc.biz;

import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.framework.core.impl.biz.GenericBizServiceImpl;
import com.kalix.mplive.mooc.api.biz.IMoocStudentRecordBeanService;
import com.kalix.mplive.mooc.api.dao.IMoocStudentRecordBeanDao;
import com.kalix.mplive.mooc.api.dto.MoocRecordDTO;
import com.kalix.mplive.mooc.entities.MoocStudentRecordBean;

import java.util.List;

/**
 * @ClassName MoocStudentRecordBeanServiceImpl
 * @Author ZhaoHang
 * @Date 2019/12/6 9:38
 */
public class MoocStudentRecordBeanServiceImpl extends GenericBizServiceImpl<IMoocStudentRecordBeanDao, MoocStudentRecordBean> implements IMoocStudentRecordBeanService {
    /**
     * 获取学生慕课的浏览记录
     * @param studentId
     * @return
     */
    @Override
    public JsonData getRecord(long studentId) {
        String sql = "select m.id,a.className,a.classCover,a.id as classId,m.inTime from mplive_moocstudentrecord m,mplive_moocapplication a where" +
                " m.studentId = "+studentId+" and m.classId = a.id";
        List<MoocRecordDTO> list = dao.findByNativeSql(sql,MoocRecordDTO.class);
        JsonData jsonData = new JsonData();
        jsonData.setData(list);
        return jsonData;
    }

    /**
     * 添加记录前判断是否已有记录
     * @param studentId
     * @param classId
     * @return
     */
    @Override
    public JsonData judgeIsExist(long studentId, long classId) {
        JsonData  jsonData= new JsonData();
        String sql = "select * from mplive_moocstudentrecord where classId = "+ classId+"and studentId = "+ studentId;
        List<MoocStudentRecordBean> moocStudentRecordList = dao.findByNativeSql(sql,MoocStudentRecordBean.class);
        jsonData.setTotalCount((long)moocStudentRecordList.size());
        jsonData.setData(moocStudentRecordList);
        return jsonData;
    }
}
