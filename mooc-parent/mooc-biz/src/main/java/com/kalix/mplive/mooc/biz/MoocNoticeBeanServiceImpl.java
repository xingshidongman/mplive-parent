package com.kalix.mplive.mooc.biz;

import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.framework.core.impl.biz.GenericBizServiceImpl;
import com.kalix.mplive.live.api.dao.ISignUpBeanDao;
import com.kalix.mplive.live.entities.SignUpBean;
import com.kalix.mplive.mooc.api.biz.IMoocNoticeBeanService;
import com.kalix.mplive.mooc.api.dao.IMoocNoticeBeanDao;
import com.kalix.mplive.mooc.entities.MoocNoticeBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName MoocNoticeBeanServiceImpl
 * @Author ZhaoHang
 * @Date 2019/11/26 15:06
 */
public class MoocNoticeBeanServiceImpl extends GenericBizServiceImpl<IMoocNoticeBeanDao, MoocNoticeBean> implements IMoocNoticeBeanService {
    /**
     * 根据教师id、教师名称、页数获取指定页数的教师通知
     * @param teacherId
     * @param teacher
     * @param page
     * @return
     */
    @Override
    public JsonData getTeacherNotice(long teacherId,String teacher,int page) {
        String sql ="select * from mplive_moocnotice where personName = '"+teacher+"' and personId = "+teacherId+" order by creationdate desc";
        List<MoocNoticeBean> list = dao.findByNativeSql(sql,MoocNoticeBean.class);
        JsonData jsonData = new JsonData();
        jsonData.setTotalCount((long)list.size());
        if(list.size()>0){
            int numPage = 0;
            if (list.size() % 6 == 0) {
                numPage = list.size() / 6;
            } else {
                numPage = list.size() / 6 + 1;
            }
            List<List<MoocNoticeBean>> listPge = new ArrayList<>();
            for (int i = 0; i < numPage; i++) {
                List<MoocNoticeBean> temp = new ArrayList<>();
                for (int j = i * 6; j < (i + 1) * 6; j++) {
                    if (j==list.size()) {
                        break;
                    }else{
                        temp.add(list.get(j));
                    }
                }
                listPge.add(temp);
            }
            jsonData.setData(listPge.get(page-1));
        }else{
            jsonData.setData(list);
        }
        return jsonData;
    }

    /**
     * 根据学生id获取学生的通知
     * @param studentId
     * @param page
     * @return
     */
    @Override
    public JsonData getStudentNotice(long studentId,int page) {
        int min = (page-1)*6;
        String sql ="select * from mplive_moocnotice where personId = "+studentId+"  order by creationdate desc limit 6 offset "+min;
        List<MoocNoticeBean> list = dao.findByNativeSql(sql,MoocNoticeBean.class);
        String sqlCount ="select * from mplive_moocnotice where personId = "+studentId;
        List<MoocNoticeBean> listCount = dao.findByNativeSql(sqlCount,MoocNoticeBean.class);
        JsonData jsonData = new JsonData();
        jsonData.setTotalCount((long)listCount.size());
        jsonData.setData(list);
        return jsonData;
    }

    /**
     * 获取新消息的数量
     * @param id
     * @return
     */
    @Override
    public Integer getNewNoticeNum(long id) {
        String sql ="select * from mplive_moocnotice where personId = '"+id+"' order by creationdate desc";
        List<MoocNoticeBean> list = dao.findByNativeSql(sql,MoocNoticeBean.class);
        int num = 0;
        for(int i=0;i<list.size();i++){
            if (list.get(i).getNew()==true) {
                num+=1;
            }
        }
        return num;
    }
    private ISignUpBeanDao signUpDao;

    public void setSignUpDao(ISignUpBeanDao signUpDao) {
        this.signUpDao = signUpDao;
    }

    /**
     *直播课是否开课对已报名学生进行通知
     * @param classId
     * @param studentNotice
     * @return
     */
    @Override
    public JsonStatus liveNotice(long classId,String studentNotice) {
        JsonStatus jsonStatus = new JsonStatus();
        String sql = null;
        try {
            sql = "select * from mplive_signup where classId = '"+ classId+"'";
            List<SignUpBean> list = signUpDao.findByNativeSql(sql, SignUpBean.class);
            for(int i = 0;i<list.size();i++){
                MoocNoticeBean moocNoticeBean = new MoocNoticeBean();
                moocNoticeBean.setNew(true);
                moocNoticeBean.setNotice(studentNotice);
                moocNoticeBean.setPersonId(list.get(i).getStudentId());
                dao.save(moocNoticeBean);
            }
            jsonStatus.setMsg("审核成功！");
            jsonStatus.setSuccess(true);
        } catch (Exception e) {
            e.printStackTrace();
            jsonStatus.setMsg("审核失败！");
            jsonStatus.setFailure(true);
        }
        return jsonStatus;
    }
}
