package com.kalix.mplive.live.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @ClassName LiveBean
 * @Author ZhaoHang
 * @Date 2019/11/14 16:12
 */
@Entity
@Table(name = "mplive_live")
public class LiveBean extends PersistentEntity {
    private String className;
    @Column(name = "classIntroduce", columnDefinition = "text")
    private String classIntroduce;
    @Column(name = "teacherIntroduce", columnDefinition = "text")
    private String teacherIntroduce;
    @Column(name = "classCover", columnDefinition = "text")
    private String classCover;
    private long teacherId;
    private String teacher;
    private long classTypeId;
    private String classType;
    private Date openTime;
    private Date closeTime;
    //是否开课
    private Integer isOpen;
    //是否通过
    private Integer isExamine;
    //是否允许评价
    private Boolean isAppraise;
    //开课人数
    private Integer personNum;
    //当前报名人数
    private Integer nowNum;
    private String liveVideo;
    //截止时间
    private Date overTime;
    @Column(name = "courseware", columnDefinition = "text")
    private String courseware;
    private Boolean isHot;
    private String reason;
    //是否推荐
    private Boolean recommend;
    private String isLiveOpen;

    public String getIsLiveOpen() {
        return isLiveOpen;
    }

    public void setIsLiveOpen(String isLiveOpen) {
        this.isLiveOpen = isLiveOpen;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public Boolean getRecommend() {
        return recommend;
    }

    public void setRecommend(Boolean recommend) {
        this.recommend = recommend;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Boolean getHot() {
        return isHot;
    }

    public void setHot(Boolean hot) {
        isHot = hot;
    }

    public String getCourseware() {
        return courseware;
    }

    public void setCourseware(String courseware) {
        this.courseware = courseware;
    }

    public Integer getNowNum() {
        return nowNum;
    }

    public void setNowNum(Integer nowNum) {
        this.nowNum = nowNum;
    }

    public Integer getIsExamine() {
        return isExamine;
    }

    public void setIsExamine(Integer isExamine) {
        this.isExamine = isExamine;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getOverTime() {
        return overTime;
    }

    public void setOverTime(Date overTime) {
        this.overTime = overTime;
    }

    public String getLiveVideo() {
        return liveVideo;
    }

    public void setLiveVideo(String liveVideo) {
        this.liveVideo = liveVideo;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassIntroduce() {
        return classIntroduce;
    }

    public void setClassIntroduce(String classIntroduce) {
        this.classIntroduce = classIntroduce;
    }

    public String getTeacherIntroduce() {
        return teacherIntroduce;
    }

    public void setTeacherIntroduce(String teacherIntroduce) {
        this.teacherIntroduce = teacherIntroduce;
    }

    public String getClassCover() {
        return classCover;
    }

    public void setClassCover(String classCover) {
        this.classCover = classCover;
    }

    public long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(long teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public long getClassTypeId() {
        return classTypeId;
    }

    public void setClassTypeId(long classTypeId) {
        this.classTypeId = classTypeId;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Integer getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(Integer isOpen) {
        this.isOpen = isOpen;
    }

    public Boolean getAppraise() {
        return isAppraise;
    }

    public void setAppraise(Boolean appraise) {
        isAppraise = appraise;
    }

    public Integer getPersonNum() {
        return personNum;
    }

    public void setPersonNum(Integer personNum) {
        this.personNum = personNum;
    }
}
