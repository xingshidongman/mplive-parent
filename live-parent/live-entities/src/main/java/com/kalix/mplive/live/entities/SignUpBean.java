package com.kalix.mplive.live.entities;

import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName SignUpBean
 * @Author ZhaoHang
 * @Date 2019/11/20 10:35
 */
@Entity
@Table(name = "mplive_signup")
public class SignUpBean extends PersistentEntity {
    private long classId;
    private long studentId;
    private Boolean connect;

    public Boolean getConnect() {
        return connect;
    }

    public void setConnect(Boolean connect) {
        this.connect = connect;
    }

    public long getClassId() {
        return classId;
    }

    public void setClassId(long classId) {
        this.classId = classId;
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }
}
