package com.kalix.mplive.live.entities;

import com.kalix.framework.core.api.persistence.PersistentEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @ClassName LiveVideoBean
 * @Author ZhaoHang
 * @Date 2019/12/25 9:23
 */
@Entity
@Table(name = "mplive_livevideo")
public class LiveVideoBean extends PersistentEntity {
    private String name;
    @Column(name = "video", columnDefinition = "text")
    private String video;
    private String teacher;
    @Column(name = "classIntroduce", columnDefinition = "text")
    private String classIntroduce;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getClassIntroduce() {
        return classIntroduce;
    }

    public void setClassIntroduce(String classIntroduce) {
        this.classIntroduce = classIntroduce;
    }
}
