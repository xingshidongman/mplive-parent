package com.kalix.mplive.live.biz;

import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.framework.core.impl.biz.GenericBizServiceImpl;
import com.kalix.mplive.live.api.biz.ILiveVideoBeanService;
import com.kalix.mplive.live.api.dao.ILiveVideoBeanDao;
import com.kalix.mplive.live.entities.LiveVideoBean;

import java.io.File;

/**
 * @ClassName LiveVideoBeanServiceImpl
 * @Author ZhaoHang
 * @Date 2019/12/25 9:38
 */
public class LiveVideoBeanServiceImpl extends GenericBizServiceImpl<ILiveVideoBeanDao, LiveVideoBean> implements ILiveVideoBeanService {
    /**
     * 添加直播的录屏视频时先判断是否已存在视频
     * @param filename
     * @return
     */
    @Override
    public JsonStatus getVideo(String filename) {
        String filePath = "D:\\java-develop\\node-file-upload-master\\fu\\" + filename;
        File file = new File(filePath);
        JsonStatus jsonStatus = new JsonStatus();
        try {
            if (file.exists() && file.isFile()) {
                jsonStatus.setSuccess(true);
                jsonStatus.setMsg("文件存在");
            } else {
                jsonStatus.setSuccess(false);
                jsonStatus.setMsg("文件不存在！");
            }
        } catch (Exception e) {
            jsonStatus.setSuccess(false);
            jsonStatus.setMsg("查找失败");
        }
        return jsonStatus;
    }

    /**
     * 删除文件夹中的视频
     * @param filename
     * @return
     */
    @Override
    public JsonStatus deleteVideo(String filename) {
        String filePath = "D:\\java-develop\\node-file-upload-master\\fu\\" + filename;
        File file = new File(filePath);
        JsonStatus jsonStatus = new JsonStatus();
        try {
            if (file.exists() && file.isFile()) {
                file.delete();
                jsonStatus.setSuccess(true);
                jsonStatus.setMsg("删除成功");
            } else {
                jsonStatus.setSuccess(false);
                jsonStatus.setMsg("文件不存在！");
            }
        } catch (Exception e) {
            jsonStatus.setSuccess(false);
            jsonStatus.setMsg("删除失败");
        }
        return jsonStatus;
    }

}
