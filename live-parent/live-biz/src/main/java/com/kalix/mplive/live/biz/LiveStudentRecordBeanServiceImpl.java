package com.kalix.mplive.live.biz;

import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.framework.core.impl.biz.GenericBizServiceImpl;
import com.kalix.mplive.live.api.biz.ILiveStudentRecordBeanService;
import com.kalix.mplive.live.api.dao.ILiveStudentRecordBeanDao;
import com.kalix.mplive.live.api.dto.LiveRecordDto;
import com.kalix.mplive.live.entities.LiveStudentRecordBean;

import java.util.List;

/**
 * @ClassName LiveStudentRecordBeanServiceImpl
 * @Author ZhaoHang
 * @Date 2019/12/19 10:42
 */
public class LiveStudentRecordBeanServiceImpl extends GenericBizServiceImpl<ILiveStudentRecordBeanDao, LiveStudentRecordBean> implements ILiveStudentRecordBeanService {
    /**
     * 获取学生直播课的浏览记录
     * @param studentId
     * @return
     */
    @Override
    public JsonData getLiveStudentRecord(long studentId) {
        JsonData jsonData = new JsonData();
        String sql = "select s.id,l.className,l.classIntroduce,l.teacherIntroduce,l.classCover,l.teacherId," +
                "l.openTime,l.closeTime,l.isOpen,l.isExamine,l.personNum,l.nowNum," +
                "l.overTime,l.reason,l.recommend,s.studentId,s.liveId,s.inTime from " +
                "mplive_liveStudentRecord s,mplive_live l where s.studentId = "+studentId+" and s.liveId = l.id";
        List<LiveRecordDto> list =dao.findByNativeSql(sql, LiveRecordDto.class);
        jsonData.setData(list);
        return jsonData;
    }

    /**
     * 判断是否已有直播课的浏览记录
     * @param studentId
     * @param liveId
     * @return
     */
    @Override
    public JsonData judgeRecordIsExist(long studentId, long liveId) {
        JsonData jsonData = new JsonData();
        String sql = "select * from mplive_liveStudentRecord where liveId = "+ liveId+"and studentId = "+ studentId;
        List<LiveStudentRecordBean> liveStudentRecordList = dao.findByNativeSql(sql,LiveStudentRecordBean.class);
        jsonData.setData(liveStudentRecordList);
        jsonData.setTotalCount((long)liveStudentRecordList.size());
        return jsonData;
    }
}
