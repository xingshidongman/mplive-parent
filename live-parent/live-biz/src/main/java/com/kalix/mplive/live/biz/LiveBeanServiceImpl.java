package com.kalix.mplive.live.biz;

import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.framework.core.impl.biz.GenericBizServiceImpl;
import com.kalix.mplive.live.api.biz.ILiveBeanService;
import com.kalix.mplive.live.api.dao.ILiveBeanDao;
import com.kalix.mplive.live.entities.LiveBean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName LiveBeanServiceImpl
 * @Author ZhaoHang
 * @Date 2019/11/14 16:32
 */
public class LiveBeanServiceImpl extends GenericBizServiceImpl<ILiveBeanDao, LiveBean> implements ILiveBeanService {
    /**
     * 获取所有正在直播的直播课
     * @param page
     * @return
     */
    @Override
    public JsonData getAllLiving(int page) {
        int max = page*6;
        int min = (page-1)*6;
        JsonData jsonData = new JsonData();
        String sqlCount = "select count(*) from mplive_live where isOpen = 1";
        String sql = "select * from mplive_live where isOpen = 1  order by nowNum desc limit 6 offset "+min;
//        List<List<LiveBean>> list = getAllLive(sql);
        List<LiveBean> list= dao.findByNativeSql(sql,LiveBean.class);
        List<Integer> listCount = dao.findByNativeSql(sqlCount,Integer.class);
        jsonData.setTotalCount((long)listCount.get(0));
//        if(list.size()>0){
//            jsonData.setData(list.get(page - 1));
//        }else{
//            jsonData.setData(list);
//        }
        jsonData.setData(list);
        return jsonData;
    }

    /**
     * 根据sql获取直播中或报名中的课程
     * @param sql
     * @return
     */
    public List<List<LiveBean>> getAllLive(String sql) {
        List<LiveBean> list = dao.findByNativeSql(sql,LiveBean.class);
        JsonData jsonData = new JsonData();
        jsonData.setTotalCount((long)list.size());
        List<List<LiveBean>> listPge = new ArrayList<>();
        if(list.size()>0){
            int numPage = 0;
            if (list.size() % 3 == 0) {
                numPage = list.size() / 3;
            } else {
                numPage = list.size() / 3 + 1;
            }
            for (int i = 0; i < numPage; i++) {
                List<LiveBean> temp = new ArrayList<>();
                for (int j = i * 3; j < (i + 1) * 3; j++) {
                    if (j==list.size()) {
                        break;
                    }
                    if(j<3){
                        list.get(j).setHot(true);
                    }else{
                        list.get(j).setHot(false);
                    }
                    temp.add(list.get(j));
                }
                listPge.add(temp);
            }
        }
        return listPge;
    }

    /**
     * 获取所有报名中的课程
     * @param page
     * @return
     */
    @Override
    public JsonData getAllLiveSigning(int page) {
        int max = page*6;
        int min = (page-1)*6;
        JsonData jsonData = new JsonData();
        String sqlCount = "select count(*) from mplive_live where isExamine = 1 and isOpen =0 ";
        String sql = "select * from mplive_live where isExamine = 1 and isOpen =0 order by nowNum desc limit 6 offset "+min;
//        List<List<LiveBean>> list = getAllLive(sql);
        List<LiveBean> list= dao.findByNativeSql(sql,LiveBean.class);
        List<Integer> listCount = dao.findByNativeSql(sqlCount,Integer.class);
        if(listCount.size()>0){
            jsonData.setTotalCount((long)listCount.get(0));
        }
        jsonData.setData(list);
        return jsonData;
    }

    /**
     * 获取老师的直播课程
     * @param teacherId
     * @return
     */
    @Override
    public JsonData getLiveByTeacherId(long teacherId) {
        JsonData jsonData = new JsonData();
        String sql = "select * from mplive_live where teacherId = "+ teacherId;
        List<LiveBean> list = dao.findByNativeSql(sql,LiveBean.class);
        jsonData.setTotalCount((long)list.size());
        if(list.size()>0){
            jsonData.setData(list);
        }
        return jsonData;
    }

    /**
     * 老师申请直播课提交前判断时间是否在已有直播课程之后
     * @param openTime
     * @param closeTime
     * @param id
     * @return
     */
    @Override
    public JsonStatus judgeTime(String openTime, String closeTime,long id) {
        JsonStatus jsonStatus = new JsonStatus();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
        String date = null;
        try {
            date = sdf.format(sdf.parse(openTime));
            String sql = "select max(closetime) from  mplive_live where date(closetime) = '"+date+"' and teacherId = "+id;
//            String sql = "select opentime from  mplive_live where opentime = '"+date+"' and teacherId = "+id;
            List<String> list = dao.findByNativeSql(sql,String.class);
            SimpleDateFormat sdformat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            if(list.get(0)==null){
                jsonStatus.setSuccess(true);
            }else{
                if(sdformat.parse(list.get(0)).before(sdformat.parse(openTime))){
                    jsonStatus.setSuccess(true);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonStatus;
    }

    /**
     * 获取首页推荐的直播课
     * @return
     */
    @Override
    public JsonData getRecommend() {
        JsonData jsonData = new JsonData();
        String sql = "select * from mplive_live where isExamine = 1 and recommend = true";
        List<LiveBean> list = dao.findByNativeSql(sql,LiveBean.class);
        jsonData.setData(list);
        return jsonData;
    }

    @Override
    public JsonData getLiveCourse(long id, long teacherId) {
        String sql = "select * from mplive_live where id = '"+id+"'  and  teacherId = '"+teacherId+"' ";
        List<LiveBean> list = dao.findByNativeSql(sql,LiveBean.class);
        JsonData jsonData = new JsonData();
        jsonData.setData(list);
        return jsonData;
    }

}
