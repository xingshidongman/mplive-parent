package com.kalix.mplive.live.biz;

import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.impl.biz.GenericBizServiceImpl;
import com.kalix.mplive.live.api.biz.ISignUpBeanService;
import com.kalix.mplive.live.api.dao.ISignUpBeanDao;
import com.kalix.mplive.live.entities.SignUpBean;

import java.util.List;

/**
 * @ClassName SignUpBeanServiceImpl
 * @Author ZhaoHang
 * @Date 2019/11/20 10:53
 */
public class SignUpBeanServiceImpl extends GenericBizServiceImpl<ISignUpBeanDao, SignUpBean> implements ISignUpBeanService {
    /**
     * 判断是否重复报名
     * @param studentId
     * @param classId
     * @return
     */
    @Override
    public Boolean judgeSignUp(long studentId,long classId) {
        Boolean bool = false;
        String sql = "select * from mplive_signup where studentId = "+ studentId +"and classId = "+classId;
        List<SignUpBean> signUpBeanList = dao.findByNativeSql(sql, SignUpBean.class);
        if(signUpBeanList.size()>0){
            bool = true;
        }
        return bool;
    }

    /**
     * 获取直播已经连麦学生id
     * @param classId
     * @return
     */
    @Override
    public JsonData getConnect(Integer classId) {
        String sql = "select studentId from mplive_signup where connect = true and classId ="+classId;
        List<Long> ListConnect = dao.findByNativeSql(sql,long.class);
        JsonData jsonData = new JsonData();
        jsonData.setData(ListConnect);
        return jsonData;
    }

    /**
     * 老师连麦学生
     * * @param studentId
     * @param classId
     */
    @Override
    public void connectStudent(Integer studentId,Integer classId) {
        String sql = "update  mplive_signup set connect = true where studentId ="+studentId +" and classId="+classId;
        dao.updateNativeQuery(sql);
    }

    /**
     * 老师取消连麦学生
     * @param studentId
     * @param classId
     */
    @Override
    public void closeConnectStudent(Integer studentId,Integer classId) {
        String sql = "update  mplive_signup set connect = false where studentId ="+studentId+" and classId="+classId;
        dao.updateNativeQuery(sql);
    }

    /**
     * 关闭直播取消取消所有连麦
     * @param classId
     */
    @Override
    public void closeAllConnectStudent(Integer classId) {
        String sql = "update  mplive_signup set connect = false where classId="+classId;
        dao.updateNativeQuery(sql);
    }
}
