package com.kalix.mplive.live.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.live.api.dao.ISignUpBeanDao;
import com.kalix.mplive.live.entities.SignUpBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @ClassName SignUpBeanDaoImpl
 * @Author ZhaoHang
 * @Date 2019/11/20 10:49
 */
public class SignUpBeanDaoImpl extends GenericDao<SignUpBean, Long> implements ISignUpBeanDao {
    @Override
    @PersistenceContext(unitName = "live-unit")
    public void setEntityManager(EntityManager em) {
        super.setEntityManager(em);
    }
}
