package com.kalix.mplive.live.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.live.api.dao.ILiveVideoBeanDao;
import com.kalix.mplive.live.entities.LiveVideoBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @ClassName LiveVideoBeanDaoImpl
 * @Author ZhaoHang
 * @Date 2019/12/25 9:35
 */
public class LiveVideoBeanDaoImpl extends GenericDao<LiveVideoBean, Long> implements ILiveVideoBeanDao {
    @Override
    @PersistenceContext(unitName = "live-unit")
    public void setEntityManager(EntityManager em) {
        super.setEntityManager(em);
    }
}
