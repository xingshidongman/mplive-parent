package com.kalix.mplive.live.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.live.api.dao.ILiveStudentRecordBeanDao;
import com.kalix.mplive.live.entities.LiveStudentRecordBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @ClassName LiveStudentRecordBeanDaoImpl
 * @Author ZhaoHang
 * @Date 2019/12/19 10:40
 */
public class LiveStudentRecordBeanDaoImpl extends GenericDao<LiveStudentRecordBean, Long> implements ILiveStudentRecordBeanDao {
    @Override
    @PersistenceContext(unitName = "live-unit")
    public void setEntityManager(EntityManager em) {
        super.setEntityManager(em);
    }
}
