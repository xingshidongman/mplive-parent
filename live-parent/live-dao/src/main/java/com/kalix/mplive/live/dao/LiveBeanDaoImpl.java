package com.kalix.mplive.live.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.live.api.dao.ILiveBeanDao;
import com.kalix.mplive.live.entities.LiveBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @ClassName LiveBeanDaoImpl
 * @Author ZhaoHang
 * @Date 2019/11/14 16:28
 */
public class LiveBeanDaoImpl extends GenericDao<LiveBean, Long> implements ILiveBeanDao {
    @Override
    @PersistenceContext(unitName = "live-unit")
    public void setEntityManager(EntityManager em) {
        super.setEntityManager(em);
    }
}
