package com.kalix.mplive.live.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.live.entities.LiveVideoBean;

public interface ILiveVideoBeanDao extends IGenericDao<LiveVideoBean,Long> {
}
