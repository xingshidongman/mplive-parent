package com.kalix.mplive.live.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.live.entities.LiveStudentRecordBean;

public interface ILiveStudentRecordBeanDao extends IGenericDao<LiveStudentRecordBean,Long> {
}
