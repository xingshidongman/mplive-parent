package com.kalix.mplive.live.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.mplive.live.entities.LiveVideoBean;

public interface ILiveVideoBeanService extends IBizService<LiveVideoBean> {
    JsonStatus getVideo(String filename);
    JsonStatus deleteVideo(String filename);
}
