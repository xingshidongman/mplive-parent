package com.kalix.mplive.live.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.mplive.live.entities.LiveStudentRecordBean;

public interface ILiveStudentRecordBeanService extends IBizService<LiveStudentRecordBean> {
    JsonData getLiveStudentRecord(long studentId);
    JsonData judgeRecordIsExist(long studentId,long liveId);
}
