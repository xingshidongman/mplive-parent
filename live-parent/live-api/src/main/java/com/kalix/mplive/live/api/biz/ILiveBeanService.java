package com.kalix.mplive.live.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.framework.core.api.persistence.JsonStatus;
import com.kalix.mplive.live.entities.LiveBean;


public interface ILiveBeanService extends IBizService<LiveBean> {
    JsonData getAllLiving(int page);
    JsonData getAllLiveSigning(int page);
    JsonData getLiveByTeacherId(long teacherId);
    JsonStatus judgeTime(String openTime,String closeTime,long id);
    JsonData getRecommend();
    // 获取直播间的课件，关联直播间id和教师id
    JsonData getLiveCourse(long id, long teacherId);
}
