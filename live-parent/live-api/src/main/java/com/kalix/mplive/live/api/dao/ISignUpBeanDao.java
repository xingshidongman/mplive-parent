package com.kalix.mplive.live.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.live.entities.SignUpBean;

public interface ISignUpBeanDao extends IGenericDao<SignUpBean,Long> {
}
