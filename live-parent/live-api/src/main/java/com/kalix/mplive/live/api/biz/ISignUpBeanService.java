package com.kalix.mplive.live.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.framework.core.api.persistence.JsonData;
import com.kalix.mplive.live.entities.SignUpBean;


public interface ISignUpBeanService extends IBizService<SignUpBean> {
    Boolean judgeSignUp(long studentId,long classId);
    JsonData getConnect(Integer classId);
    void connectStudent(Integer studentId,Integer classId);
    void closeConnectStudent(Integer studentId,Integer classId);
    public void closeAllConnectStudent(Integer classId);
}
