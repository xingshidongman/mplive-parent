package com.kalix.mplive.about.dao;

import com.kalix.framework.core.impl.dao.GenericDao;
import com.kalix.mplive.about.api.dao.IConcerningBeanDao;
import com.kalix.mplive.about.entities.ConcerningBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 功能描述：关于
 *
 * @Author: Wanghw
 * @Date: 2019/11/12
 * @Version V1.0
 *
**/
public class ConcerningBeanDaoImpl extends GenericDao<ConcerningBean, Long> implements IConcerningBeanDao {
    @Override
    @PersistenceContext(unitName = "mplive-about-unit")
    public void setEntityManager(EntityManager em) {

        super.setEntityManager(em);
    }
}
