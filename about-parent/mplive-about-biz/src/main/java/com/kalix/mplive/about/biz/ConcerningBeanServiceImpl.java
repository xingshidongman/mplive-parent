package com.kalix.mplive.about.biz;

import com.kalix.framework.core.impl.biz.ShiroGenericBizServiceImpl;
import com.kalix.mplive.about.api.biz.IConcerningBeanService;
import com.kalix.mplive.about.api.dao.IConcerningBeanDao;
import com.kalix.mplive.about.entities.ConcerningBean;


/**
 * 功能描述：关于
 *
 * @Author: Wanghw
 * @Date: 2019/11/12
 * @Version V1.0
 *
**/
public class ConcerningBeanServiceImpl extends ShiroGenericBizServiceImpl<IConcerningBeanDao, ConcerningBean> implements IConcerningBeanService {
}
