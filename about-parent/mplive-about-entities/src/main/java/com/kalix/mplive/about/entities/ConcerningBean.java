package com.kalix.mplive.about.entities;

import com.kalix.framework.core.api.persistence.PersistentEntity;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 功能描述：关于
 *
 * @Author: Wanghw
 * @Date: 2019/11/12
 * @Version V1.0
 *
**/
@Entity
@Table(name = "mplive_concerning")
public class ConcerningBean extends PersistentEntity {
    private String title;  // 标题
    private String content; // 内容
    private String photo; // 图片
    private String email; //邮箱
    private String telephone; // 电话
    private String fax; // 传真
    private String addr;// 地址

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }
}

