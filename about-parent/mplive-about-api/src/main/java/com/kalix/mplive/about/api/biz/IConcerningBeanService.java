package com.kalix.mplive.about.api.biz;

import com.kalix.framework.core.api.biz.IBizService;
import com.kalix.mplive.about.entities.ConcerningBean;

/**
 * 功能描述：关于
 *
 * @Author: Wanghw
 * @Date: 2019/11/12
 * @Version V1.0
 *
**/
public interface IConcerningBeanService extends IBizService<ConcerningBean> {
}
