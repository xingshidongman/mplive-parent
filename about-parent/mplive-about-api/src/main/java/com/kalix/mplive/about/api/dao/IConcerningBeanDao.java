package com.kalix.mplive.about.api.dao;

import com.kalix.framework.core.api.dao.IGenericDao;
import com.kalix.mplive.about.entities.ConcerningBean;

/**
 * 功能描述：关于
 *
 * @Author: Wanghw
 * @Date: 2019/11/12
 * @Version V1.0
 *
**/
public interface IConcerningBeanDao extends IGenericDao<ConcerningBean, Long> {
}
